package cylinder;

import com.intendico.gorite.Performer;

/**
 * The HCylinder class models the combined behaviour capabilities to
 * fill any of the roles of in a work peace delivery.
 */
public class Gripper extends Performer {

    /**
     * Constructor, which adds required goals.
     */
    public Gripper(String agentName, String deviceIp, int devicePort, int agentPort) {
        super( agentName );
        addGoal( new PickingUp( "picking up work piece", deviceIp, devicePort, agentPort, agentName));
    }
}
