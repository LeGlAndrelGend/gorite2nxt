package cylinder;


import com.intendico.gorite.Data;
import com.intendico.gorite.Goal;

import static cylinder.Main.GRIPPERSTATE;

/**
 * The MovingCyl class implements a capability to achieve the
 * {@link Main#PICKUP} goal.
 */
public class PickingUp extends Goal {

    /**
     * Constructor, which adds a plan to achieve {@link Main#PICKUP}.
     */

	String deviceIp = "null";
	int devicePort = -1;
	int agentPort = -1;
	String agentName = "The agentName of agent";
    public PickingUp(String goalName, String deviceIp, int devicePort, int agentPort, String agentName) {
    	super(goalName);
		this.deviceIp = deviceIp;
		this.devicePort = devicePort;
		this.agentPort = agentPort;
    	this.agentName = agentName;
    }
    
	public States execute(Data d) {
		String state = d.getValue(GRIPPERSTATE).toString();

		UDPClient client = new UDPClient("device with IP:port" + deviceIp + ":" + devicePort, deviceIp, devicePort, agentPort, state);
		System.out.println("Agent: " + agentName + ", has been sent control signal('" + state + "') to device.");
		return States.PASSED;
	}
}
