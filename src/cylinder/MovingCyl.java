package cylinder;
import com.intendico.gorite.*;

import java.util.HashMap;
import java.util.Map;
import static cylinder.Main.HCYLPARAMS;
import static cylinder.Main.VCYLPARAMS;

/**
 * The MovingCyl class implements a capability to achieve the
 * {@link Main#MOVECYL} goal.
 */
public class MovingCyl extends Goal {

    /**
     * Constructor, which adds a plan to achieve {@link Main#MOVECYL}.
     */

    String deviceIp = "null";
	int devicePort = -1;
	int agentPort = -1;
    String agentName = "The agentName of agent";
    public MovingCyl(String goalName, String deviceIp, int devicePort, int agentPort, String agentName) {
    	super(goalName);
		this.deviceIp = deviceIp;
		this.devicePort = devicePort;
		this.agentPort = agentPort;
    	this.agentName = agentName;
    }

	public States execute(Data d) {

		Map<String, Object> agentParams = new HashMap<>();
		if (agentName.contains("HCyl"))
			agentParams = HCYLPARAMS.get(agentName);
		else if (agentName.contains("VCyl"))
			agentParams = VCYLPARAMS.get(agentName);

		if(agentParams.get("state") == null) {
			System.out.println("Agent: " + agentName + ", is not scheduled, sending the control signals has been canceled.");
			return States.FAILED;
		}
		String state = agentParams.get("state").toString();

		UDPClient client = new UDPClient("device with IP:port" + deviceIp + ":" + devicePort, deviceIp, devicePort, agentPort, state);
		System.out.println("Agent: " + agentName + ", has been sent control signal('" + state + "') to device.");
		//			if (client.responceMsg.contains("success"))
//				return States.PASSED;
//			else
//				return States.FAILED;

		return States.PASSED;
    }
}
