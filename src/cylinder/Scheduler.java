package cylinder;

import java.util.*;

public class Scheduler {
    List<List<Integer>> validCombins = new ArrayList<>();
    List<Integer> lengths = null;
    int targetValue = -1;
    public Scheduler(List<Integer> lengths, int targetValue) {
        this.lengths = lengths;
        this.targetValue = targetValue;
        System.out.println("Scheduling has been initialized...");
    }

    /** description
     * arr[]  ---> Input Array
     * data[] ---> Temporary array to store current combination
     * start & end ---> Staring and Ending indexes in arr[]
     * index  ---> Current index in data[]
     * r ---> Size of a combination to be printed */
    void combinationUtil(List<Integer> arr, int data[], int start,
                                int end, int index, int r, int targetValue)
    {
        List<Integer> combination = new ArrayList<>();
        int sum = 0;
        // Current combination is ready to be printed, print it
        if (index == r)
        {
            for (int j=0; j<r; j++){
//                System.out.print(data[j]+" ");
                sum += data[j];
                combination.add(data[j]);
            }
//            System.out.println("");
            if (sum == targetValue && !validCombins.contains(combination)){
                validCombins.add(combination);
            }
            return;
        }

        // replace index with all possible elements. The condition
        // "end-i+1 >= r-index" makes sure that including one element
        // at index will make a combination with remaining elements
        // at remaining positions
        for (int i=start; i<=end && end-i+1 >= r-index; i++)
        {
            data[index] = arr.get(i);
            combinationUtil(arr, data, i+1, end, index+1, r, targetValue);
        }
    }

    // The main function that prints all combinations of size r
    // in arr[] of size n. This function mainly uses combinationUtil()
    void findValidCombs(int r, int targetValue)
    {
        // A temporary array to store all combination one by one
        int data[]=new int[r];

        // Print all combination using temporary array 'data[]'
        combinationUtil(lengths, data, 0, lengths.size() - 1, 0, r, targetValue);
    }

    public List<Integer> getValidComb() {

        /** get all valid combinations*/
        int n = lengths.size();
        for (int i = 0; i <= n; i++) {
            findValidCombs(i, targetValue);
        }
        if (validCombins.isEmpty()) {
            return null;
        }
        return(validCombins.get(0));
    }
}
