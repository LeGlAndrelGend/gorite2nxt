package cylinder;

import com.intendico.gorite.Data;
import com.intendico.gorite.Goal;
import org.json.JSONObject;

import java.util.HashMap;

import static cylinder.Main.HCYLPARAMS;
import static cylinder.Main.VCYLPARAMS;


public class GettingCylLength extends Goal {

    String deviceIp = "null";
    int devicePort = -1;
    int agentPort = -1;
    String agentName;
    public GettingCylLength(String goalName, String deviceIp, int devicePort, int agentPort, String agentName) {
        super(goalName);
        this.deviceIp = deviceIp;
        this.devicePort = devicePort;
        this.agentPort = agentPort;
        this.agentName = agentName;
    }

    public States execute(Data d) {

        UDPClient client = new UDPClient("device with IP:port" + deviceIp + ":" + devicePort, deviceIp, devicePort, agentPort, "getLength()");

        String responce = client.responceMsg.toString();
        JSONObject jsonObject = new JSONObject(responce);
        int length = jsonObject.getInt("length");

        if (agentName.contains("HCyl")){
            if(!HCYLPARAMS.containsKey(agentName))
                HCYLPARAMS.put(agentName, new HashMap<>());
            HCYLPARAMS.get(agentName).put("length", length);
            HCYLPARAMS.get(agentName).put("state", null);
        } else if(agentName.contains("VCyl")) {
            if(!VCYLPARAMS.containsKey(agentName))
                VCYLPARAMS.put(agentName, new HashMap<>());
            VCYLPARAMS.get(agentName).put("length", length);
            VCYLPARAMS.get(agentName).put("state", null);
        }

        System.out.println("Goal:" + getGoalName() + "; agent name:port:" + agentName + ":" + agentPort + "; lengths:" + length);

        return States.PASSED;
    }
}
