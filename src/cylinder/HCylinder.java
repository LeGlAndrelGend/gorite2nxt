package cylinder;

import com.intendico.gorite.Performer;


/**
 * The HCylinder class models the combined behaviour capabilities to
 * fill any of the roles of in a work peace delivery.
 */
public class HCylinder extends Performer {

    /**
     * Constructor, which adds required goals.
     */
    public HCylinder(String agentName, String deviceIp, int devicePort, int agentPort) {
        super( agentName );
        addGoal( new MovingCyl( "extend/retract cylinder", deviceIp, devicePort, agentPort, agentName));
	    addGoal( new GettingCylLength("collect length of cylinder", deviceIp, devicePort, agentPort, agentName));
    }
}
