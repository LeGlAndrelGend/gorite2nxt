package cylinder;

import com.intendico.gorite.*;

import static cylinder.Main.*;

/**
 * The PnPManipulator class models the PnPManipulator team behaviour.
 */
public class PnPManipulator extends Team {

    /**
     * Constructor, which adds a method to achieve {@link Main#DELIVERY}.
     */
    public PnPManipulator(String name) {

        super( name );

		setTaskTeam( "collectors", 	new TaskTeam() { {
			addRole( new Role(FirstHMover, 	new String [] { MOVECYL, GETLENGTH } ) );
			addRole( new Role(SecondHMover, new String [] { MOVECYL, GETLENGTH } ) );
			addRole( new Role(FirstVMover, 	new String [] { MOVECYL, GETLENGTH } ) );
			addRole( new Role(SecondVMover, new String [] { MOVECYL, GETLENGTH } ) );
			addRole( new Role(Updater, 	new String [] {SCHEDULE} ) );
		} } );

		setTaskTeam( "delivery", 	new TaskTeam() { {
			addRole( new Role(FirstHMover, 	new String [] { MOVECYL, GETLENGTH } ) );
			addRole( new Role(SecondHMover, new String [] { MOVECYL, GETLENGTH } ) );
			addRole( new Role(FirstVMover, 	new String [] { MOVECYL, GETLENGTH } ) );
			addRole( new Role(SecondVMover, new String [] { MOVECYL, GETLENGTH } ) );
			addRole( new Role(Gripper, 	new String [] { PICKUP } ) );
			addRole( new Role(Updater, 	new String [] {SCHEDULE} ) );
		} } );

		addGoal( new SequenceGoal(COLLECTDATA, new Goal []{
				deploy("collectors"),
				new SequenceGoal("collect info and generate schedule", new Goal[]{
						new TeamGoal(Updater, GETCOORDINATES),
						new TeamGoal(FirstHMover, GETLENGTH),
						new TeamGoal(SecondHMover, GETLENGTH),
						new TeamGoal(FirstVMover, GETLENGTH),
						new TeamGoal(SecondVMover, GETLENGTH),
						new TeamGoal(Updater, SCHEDULE),
				}),
		}));

		addGoal( new SequenceGoal(DELIVERY, new Goal[] {
				deploy("delivery"),
				new SequenceGoal( "get work piece and put to conveyor", new Goal [] {
						new TeamGoal(FirstHMover, MOVECYL),
						new TeamGoal(SecondHMover, MOVECYL),
						new TeamGoal(FirstVMover, MOVECYL),
						new TeamGoal(SecondVMover, MOVECYL),
						new TeamGoal(Gripper, PICKUP),
				} ),
		} ) );
    }

    public void operatorPickNPlace()
	{
		/**Setup data providers,
		 * required for access to Data context otherwise attempt to get param return null*/
		Data data = new Data();
		data.setValue(XTARGET, -1);
		data.setValue(YTARGET, -1);
		data.setValue(GRIPPERSTATE, true);

		performGoal( new BDIGoal(COLLECTDATA), "call collecting data", data);
		performGoal( new BDIGoal(DELIVERY), "call delivery", data);
	}
}
