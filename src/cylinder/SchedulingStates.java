package cylinder;

import com.intendico.gorite.Data;
import com.intendico.gorite.Goal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static cylinder.Main.*;
import static cylinder.Main.VCYLPARAMS;

public class SchedulingStates extends Goal {

    String agentName;
    public SchedulingStates(String goalName, String agentName){
        super(goalName);
        this.agentName = agentName;
    }

    public States execute(Data d) {
        System.out.println("Goal: " + getGoalName() + "; Agent: " + agentName + "; Scheduling process has been lunched...");
        int xTarget = (Integer) d.getValue(XTARGET);
        List<Integer> HCylLengths = new ArrayList<>();
        for (Map.Entry agent : HCYLPARAMS.entrySet()) {
            Map<String, Object> params = (Map<String, Object>)agent.getValue();
            HCylLengths.add((Integer)params.get("length"));
        }

        Scheduler schedulerX = new Scheduler(HCylLengths, xTarget);
        List<Integer> validCombX = schedulerX.getValidComb();
        if (validCombX == null){
            System.err.println("Error goal: There is no valid HORIZONTAL combination, goal is not reached");
            return States.FAILED;
        } else {
            System.out.println("Set enable state for HCylinders: ");
            for (Map.Entry agent : HCYLPARAMS.entrySet()) {
                Map<String, Object> params = (Map<String, Object>)agent.getValue();
                params.put("state", false);
            }

            for (int curComb = 0; curComb < validCombX.size(); curComb++) {
                int length = validCombX.get(curComb);
                for (Map.Entry agent : HCYLPARAMS.entrySet()) {
                    Map<String, Object> params = (Map<String, Object>)agent.getValue();
                    if((Integer)params.get("length") == length && !(Boolean)params.get("state")) {
                        params.put("state", true);
                        System.out.println("Agent: " + agent.getKey() + ", enabled for action");
                        break;
                    }
                }
            }
        }

        int yTarget = (Integer) d.getValue(YTARGET);
        List<Integer> VCylLengths = new ArrayList<>();

        for (Map.Entry agent : VCYLPARAMS.entrySet()) {
            Map<String, Object> params = (Map<String, Object>)agent.getValue();
            VCylLengths.add((Integer)params.get("length"));
        }

        Scheduler schedulerY = new Scheduler(VCylLengths, yTarget);
        List<Integer> validCombY = schedulerY.getValidComb();
        if (validCombY == null){
            System.err.println("Error goal: There is no valid VERTICAL combination, goal is not reached");
            return States.FAILED;
        } else {
            System.out.println("Set enable state for VCylinders: ");
            for (Map.Entry agent : VCYLPARAMS.entrySet()) {
                Map<String, Object> params = (Map<String, Object>)agent.getValue();
                params.put("state", false);
            }

            for (int curComb = 0; curComb < validCombY.size(); curComb++) {
                int length = validCombY.get(curComb);
                for (Map.Entry agent : VCYLPARAMS.entrySet()) {
                    Map<String, Object> params = (Map<String, Object>)agent.getValue();
                    if((Integer)params.get("length") == length && !(Boolean)params.get("state")) {
                        params.put("state", true);
                        System.out.println("Agent: " + agent.getKey() + ", enabled for action");
                        break;
                    }
                }
            }
        }
        return States.PASSED;

    }
}
