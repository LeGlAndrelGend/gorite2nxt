package cylinder;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {

    InputStream inputStream;

    public Properties getPropValues() throws IOException {
        Properties properties = new Properties();

        try {
            String filename = "config.properties";
            File file = new File(filename);

            inputStream = file.toURI().toURL().openStream();

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + filename + "' not found in the classpath");
            }
            System.out.println("Properties has been loaded successful.");


        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }
        return properties;
    }
}
