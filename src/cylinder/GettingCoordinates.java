package cylinder;

import com.intendico.gorite.Data;
import com.intendico.gorite.Goal;
import org.json.JSONObject;

import static cylinder.Main.XTARGET;
import static cylinder.Main.YTARGET;

public class GettingCoordinates extends Goal {

    String deviceIp = "null";
    int devicePort = -1;
    int agentPort = -1;
    String agentName;
    public GettingCoordinates(String goalName, String deviceIp, int devicePort, int agentPort, String agentName) {
        super(goalName);
        this.deviceIp = deviceIp;
        this.devicePort = devicePort;
        this.agentPort = agentPort;
        this.agentName = agentName;
    }

    public States execute (Data d) {

        System.out.println("Goal: " + getGoalName() + "; Agent: " + agentName + "; Getting wp coordinates has been lunched...");
        /**GET WP coordinates from model in JSON format*/
        UDPClient client = new UDPClient("device with IP:port" + deviceIp + ":" + devicePort, deviceIp, devicePort, agentPort, "getWPCoordinates()");
        String responce = client.responceMsg.toString();
        JSONObject coordinatesJSON = new JSONObject(responce);
        System.out.println("WP coordinates is " + coordinatesJSON.toString());
        int xTarget = coordinatesJSON.getInt("x");
        int yTarget = coordinatesJSON.getInt("y");
        d.setValue(XTARGET, xTarget);
        d.setValue(YTARGET, yTarget);
        return States.PASSED;
    }
}
