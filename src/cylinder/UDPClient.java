package cylinder;

import java.io.IOException;
import java.net.*;

import static com.intendico.gorite.Goal.isTracing;
import static cylinder.Main.properties;

public class UDPClient {

    String deviceId;
    String deviceIpAddr;
    int agentIpPort;
    int deviceIpPort;
    String msgToDevice;
    String responceMsg;

    public UDPClient(String deviceId, String deviceIpAddr, int deviceIpPort, int agentIpPort, String msgToDevice){
        this.deviceId = deviceId;
        this.deviceIpAddr = deviceIpAddr;
        this.agentIpPort = agentIpPort;
        this.deviceIpPort = deviceIpPort;
        this.msgToDevice = msgToDevice;
        try { sendMessage(); }
        catch (IOException e) {
            System.err.println("Error during send the msg: " + e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage() throws IOException, InterruptedException {
        if ( isTracing() )
            System.err.println("UDP connection with device id: " + deviceId + ", IP: " + deviceIpAddr + ":" + deviceIpPort + ", message: " + msgToDevice);
        DatagramSocket clientSocket = new DatagramSocket(agentIpPort);
        InetAddress IPAddress = InetAddress.getByName(deviceIpAddr);
        String sentence = msgToDevice;
        byte[] sendData = sentence.getBytes();
        byte[] receiveData = new byte[1024];

        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, deviceIpPort);
        clientSocket.send(sendPacket);
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        int requesTimeout = Integer.parseInt(properties.getProperty("requestimeout"));
        clientSocket.setSoTimeout(requesTimeout * 1000);
        clientSocket.receive(receivePacket);
        String modifiedSentence = new String(receivePacket.getData());
        if ( isTracing() )
            System.err.println("FROM SERVER message:" + modifiedSentence);
        responceMsg = modifiedSentence;

        /** Timeout in ms, waiting passing signal between FB's*/
        int requestDelay = Integer.parseInt(properties.getProperty("requestdelay"));
        Thread.sleep(requestDelay * 1000);
        clientSocket.close();
    }

/*    public static void main(String [] args) throws Throwable {
        UDPClient client = new UDPClient("device with IP:port" + "127.0.0.1" + ":" + 9876, "127.0.0.1", 55876, 9876, "getWPCoordinates()");
    }*/
}
