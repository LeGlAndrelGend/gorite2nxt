package cylinder;

import com.intendico.gorite.*;
import org.json.JSONObject;

import java.io.File;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.*;

public class Main {
    public static Properties properties = new Properties();

    public static void main(String [] args)
    throws Throwable
    {

        ConfigReader configReader = new ConfigReader();
        properties = configReader.getPropValues();

        int serverPort = Integer.parseInt(properties.getProperty("serverport"));

        String deviceIp1 = properties.getProperty("deviceip1");
        int devicePort1 = Integer.parseInt(properties.getProperty("deviceport1"));
        int agentPort1 = Integer.parseInt(properties.getProperty("agentport1"));

        String deviceIp2 = properties.getProperty("deviceip1");
        int devicePort2 = Integer.parseInt(properties.getProperty("deviceport2"));
        int agentPort2 = Integer.parseInt(properties.getProperty("agentport2"));

        String deviceIp3 = properties.getProperty("deviceip1");
        int devicePort3 = Integer.parseInt(properties.getProperty("deviceport3"));
        int agentPort3 = Integer.parseInt(properties.getProperty("agentport3"));

        String deviceIp4 = properties.getProperty("deviceip1");
        int devicePort4 = Integer.parseInt(properties.getProperty("deviceport4"));
        int agentPort4 = Integer.parseInt(properties.getProperty("agentport4"));

        String deviceIp5 = properties.getProperty("deviceip1");
        int devicePort5 = Integer.parseInt(properties.getProperty("deviceport5"));
        int agentPort5 = Integer.parseInt(properties.getProperty("agentport5"));

        String deviceIp6 = properties.getProperty("deviceip1");
        int devicePort6 = Integer.parseInt(properties.getProperty("deviceport6"));
        int agentPort6 = Integer.parseInt(properties.getProperty("agentport6"));

        DatagramSocket serverSocket = new DatagramSocket(serverPort);
        System.out.println("MAS server application has been started to listen on port: " + serverPort);
        byte[] receiveData = new byte[1024];
        byte[] sendData;
        boolean start;
        while(true)
        {
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);
            String sentence = new String( receivePacket.getData());
//            System.out.println("RECEIVED: " + sentence);
            InetAddress IPAddress = receivePacket.getAddress();
            int port = receivePacket.getPort();
            String response = "starting process...";
            sendData = response.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
            serverSocket.send(sendPacket);

            /** Execute PnPManipulator team work */

            if(sentence.contains("{start:")) {
                JSONObject jsonObject = new JSONObject(sentence);
                start = jsonObject.getBoolean("start");
                if (start) {
                    int requestDelay = Integer.parseInt(properties.getProperty("requestdelay"));
                    Thread.sleep(requestDelay * 1000);
                    /** Create the PnPManipulator team, and make it perform its delivery method.*/
                    PnPManipulator pnPManipulator = new PnPManipulator("manipulator1");

                    Performer dataUpdater = new DataUpdater("dataUpdater1", deviceIp1, devicePort1, agentPort1);
                    Performer leftHCyl =    new HCylinder("HCyl1", deviceIp2, devicePort2, agentPort2);
                    Performer middleHCyl =  new HCylinder("HCyl2", deviceIp3, devicePort3, agentPort3);
                    Performer leftVCyl =    new VCylinder("VCyl1", deviceIp4, devicePort4, agentPort4);
                    Performer middleVCyl =  new VCylinder("VCyl2", deviceIp5, devicePort5, agentPort5);
                    Performer gripper =     new Gripper("gripper1", deviceIp6, devicePort6, agentPort6);

                    pnPManipulator.addPerformer(leftHCyl);
                    pnPManipulator.addPerformer(middleHCyl);
                    pnPManipulator.addPerformer(leftVCyl);
                    pnPManipulator.addPerformer(middleVCyl);
                    pnPManipulator.addPerformer(gripper);
                    pnPManipulator.addPerformer(dataUpdater);

                    pnPManipulator.operatorPickNPlace();
                    start = false;
                }
            }

        }
    }

    // Goal names as constants
    public final static String COLLECTDATA = "collect system data";
    public final static String DELIVERY = "get work peace and put to platte";
    public final static String MOVECYL = "extend/retract cylinder";
    public final static String PICKUP = "picking up work piece";
    public final static String GETLENGTH = "collect length of cylinder";
    public final static String SCHEDULE = "update states";
    public final static String GETCOORDINATES = "get target coordinates";

    // Role names as constants
    public final static String FirstHMover = "horizontal cylinder role 1";
    public final static String SecondHMover = "horizontal cylinder role 2";
    public final static String FirstVMover = "vertical cylinder role 1";
    public final static String SecondVMover = "vertical cylinder role 2";
    public final static String Gripper = "vacuum catcher role";
    public final static String Updater = "updater of states";

    // Data names as constants
    public final static String XTARGET = "horizontal target X destination";
    public final static String YTARGET = "vertical target Y destination";
    public final static String GRIPPERSTATE = "gripper state";
    public final static Map<String, Map<String, Object>> HCYLPARAMS = new HashMap<>();
    public final static Map<String, Map<String, Object>> VCYLPARAMS = new HashMap<>();
}
