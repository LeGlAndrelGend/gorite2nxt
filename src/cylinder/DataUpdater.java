package cylinder;

import com.intendico.gorite.Performer;

    public class DataUpdater extends Performer {

        public DataUpdater(String agentName, String deviceIp, int devicePort, int agentPort) {
            addGoal(new GettingCoordinates("get target coordinates", deviceIp, devicePort, agentPort, agentName));
            addGoal(new SchedulingStates("update states", agentName));
        }
    }

