/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.gorite.Goal.States;
import com.intendico.data.*;
import java.util.Vector;

/**
 * This is a utility class used by BDIGoal execution, to inject a plan
 * choice goal for choosing the plan instance to pursue. It is not
 * intended for explicit use within goal hierarchies.
 *
 * <p> The {@link BDIGoal} uses the {@link
 * Performer#getPlanChoice(String)} method to determine whether or not
 * a goal is asoicated with a plan choice goal. If this is the case,
 * then it will create a PlanChoiceGoal instance for handling the
 * choice of which of the applicable plan options to pursue next, and
 * the subsequent execution of that option.
 *
 * <p> The choice is performed by triggering a (freschly created)
 * {@link BDIGoal} for the plan choice goal, with a separate Data
 * object that is initialised with "options", "failed" and "data". As
 * the plan choice goal succeeds, the resulting "choice" data is used
 * as being the choice of plan option, and then that plan option is
 * executed as an attempt of achieving the original goal. If that
 * succeeds, the original goal succeeds without further ado, and if it
 * fails, then the BDIGoal forms a new, possibly empty collection of
 * applicable (non-failed) plan options, and trigger a new
 * PlanChoiceGoal for that collection.
 *
 * <p> If the plan choice goal fails, then all applicable options at
 * this point are added to the failed set, the {@link BDIGoal}
 * continues by forming a new, possibly empty collection of applicable
 * (non-failed) plan options, and triggering a new PlanChoiceGoal for
 * that collection.
 *
 * <p> If the plan choice goal fails for the final triggering, i.e,
 * with an empty collection of applicable (non-failed) plan options,
 * then the original goal fails. In fact, the orginal goal fails also
 * if the final plan choice goal succeeds, except if this is also the
 * first triggering and thus, there are no failed plan options
 * either. In that case, the original goal will succeed if the plan
 * choice goal succeeds.
 *
 * @see Performer#getPlanChoice(String)
 * @see Performer#setPlanChoice(String,Object)
 */
public class PlanChoiceGoal extends Goal {

    /**
     * The invocation data.
     */
    public Data choice_data;

    /**
     * Status flag for plan choice processing.
     */
    public States done = States.STOPPED;

    /**
     * The initial first option.
     */
    public Goal first;

    /**
     * The choice goal. This is set by the plan choice processing.
     */
    public Goal choice;

    /**
     * The currently executing goal instance.
     */
    public Instance instance = null;

    /**
     * Sub goal head, which is set when this goal is instantiated
     */
    public String head;

    /**
     * Constructor for a given plan choice goal and collection of plan
     * instances.
     */
    public PlanChoiceGoal(
	Performer p,String pg,Vector/*<Goal>*/ plans,Vector/*<Goal>*/ failed) {
	super( pg );
	choice_data = new Data();
	choice_data.setValue( Goal.PERFORMER, p );
	choice_data.setValue( "options", plans );
	choice_data.setValue( "failed", failed );
	if ( plans.size() > 0 )
	    first = (Goal) plans.get( 0 );
    }

    /**
     * Instantiating this goal for execution.
     */
    public Goal.Instance instantiate(String h,Data d) {
	head = h;
	choice_data.setValue( "data", d );
	instance = new BDIGoal(
	    getGoalName() ).instantiate( h + "?", choice_data );
	return super.instantiate( h, d );
    }

    /**
     * Control callback whereby the goal gets notified that an (its)
     * intention is cancelled. This will forward the cancellation to
     * the currently progressing instance, if any.
     */
    public void cancelled(Instance which) {
	if ( instance != null )
	    instance.cancel();
    }

    /**
     * Excution of this goal. If choice is null, then the execution is
     * like a BDIGoal of the plan choice goal, with its data holding
     * the "options", for setting the "choice". If that execution is
     * successful, then the choice is taken, otherwise the original
     * first choice is taken. If choice is set, then the execution
     * executes that goal.
     */
    public States execute(Data data)
	throws ParallelEndException {
	for ( ;; ) {
	    States s = States.FAILED;
	    try {
		s = instance.perform( choice == null? choice_data: data );
	    } catch (LoopEndException e) {
		s = States.FAILED;
	    } catch (ParallelEndException e) {
		s = States.FAILED;
		if ( done != States.STOPPED )
		    throw e;
	    }
	    if ( done != States.STOPPED )
		return s;
	    if ( s != States.PASSED && s != States.FAILED )
		return s;
	    done = s;
	    if ( s == States.FAILED ) {
		Vector/*<Goal>*/ options = (Vector/*<Goal>*/)
		    choice_data.getValue( "options" );
		Vector/*<Goal>*/ failed = (Vector/*<Goal>*/)
		    choice_data.getValue( "failed" );
		failed.addAll( options );
		choice = first;
		return States.FAILED;
	    }
	    choice = (Goal) instance.getValue( "choice" );
	    if ( choice == null )
		choice = first;
	    if ( choice == null ) {
		Vector/*<Goal>*/ failed = (Vector/*<Goal>*/)
		    choice_data.getValue( "failed" );
		return failed.size() == 0? States.PASSED : States.FAILED;
	    }
	    instance = choice.instantiate( head + "!", data );
	}
    }

}
