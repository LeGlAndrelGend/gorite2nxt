/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

/**
 * The Action class is a utility base class for implementation of task
 * goals that operate on a common entity. The created action object
 * becomes a factory for goals that refer back to it, and that end up
 * invoking the Action's {@link #execute} method as way of achieving
 * the task goal.
 *
 * <p> Generally, an action is a named definition of a function
 * performed on data. It would occur in a process model in terms of a
 * task goal, which is recognised as a usage point of the action,
 * linking it to particular input and output data. The same action may
 * be used elsewhere, with other data names.
 *
 * <p> Task goals referring to the same action object will invoke the
 * same {@link #execute} method.
 *
 * <p>Example of use:
 * <pre>
 * public class MachineControlAdapter {
 *     Action open_lid = new Action( "open lid" ) {
 *         public boolean execute(
 *             boolean reentry,Data.Element [] ins,Data.Element [] * outs ) {
 *                 .. code to perform the "open lid" action
 *             }
 *     }
 *     Action status = new Action( "check status" ) {
 *         public boolean execute(
 *             boolean reentry,Data.Element [] ins,Data.Element [] * outs ) {
 *                 .. code to perform the "check status" action
 *             }
 *     }
 *     public Capability actions() {
 *         return new Capability() {{
 *             open_lid.create( new String [] { "angle" }, null );
 *             on_off.create( null, new String [] { "status" } );
 *         }};
 *     }
 * }
 * </pre>
 *
 * The example code illustrates a "machine adapter" with two actions:
 * to "open lid" to a certain "angle", and to "check status" giving a
 * "status". The "machine adapter" includes an "actions()" method that
 * creates a control capability of two goals for invoking the actions.
 */
public class Action {

    /**
     * The name of the action. This is also the name of the goal for
     * the action created via the {@link #create} method.
     */
    public String name;

    /**
     * Nominates the {@link Performer.TodoGroup} to use for performing
     * this action.
     */
    private String group;

    /**
     * Constructor without group.
     */
    public Action(String n) {
	name = n;
    }

    /**
     * Constructor.
     */
    public Action(String n,String g) {
	name = n;
	group = g;
    }

    /**
     * This is a utility class to represent the appearance of the
     * action within a goal hierarchy. Each such apprearance involves
     * its own data connections, which are contained within the Usage
     * class. When this task goal is executed, it firstly ensures that
     * all input data is available, or otherwise delays the execution
     * until they are available. Thereafter it ensures that some
     * output data needs to be produces, before invoking the action's
     * exectution. If all output data is already produced, the task
     * goal execution succeeds immediately without the actual action
     * execution.
     */
    public class Usage extends Goal {

	/**
	 * The names of data elements that are inputs to the goal; the
	 * performance of a goal will be postponed until all inputs
	 * are available, although maybe not ready.
	 */
	public String [] inputs;

	/**
	 * The names of data elements that are outputs from the
	 * action. The task goal for an action considers these: if
	 * they are all ready and available when the goal is
	 * performed, then the action execution is skipped, and the
	 * goal terminates successfully.  However, an action without
	 * outputs is always executed.
	 */
	public String [] outputs;

	/**
	 * Constructor.
	 */
	public Usage(String [] ins,String [] outs) {
	    super( Action.this.name );
	    inputs = ins;
	    outputs = outs;
	    group = getGoalGroup();
	}

	/**
	 * Creates and returns an instance object for achieving
	 * a Usage.
	 */
	public Instance instantiate(String head,Data d) {
	    return new Invoke( head, d );
	}

	/**
	 * The Invoke class represents the intention to performa Usage
	 * goal.
	 */
	public class Invoke extends Instance {

	    /**
	     * Constructor.
	     */
	    public Invoke(String head,Data d) {
		super( head );
	    }

	    /**
	     * Tracking of first or sub sequence perform calls.
	     */
	    public boolean reentry = false;

	    /**
	     * The execution of an action involves an initial step of
	     * establishing the data connections. This will postpone the
	     * actual action execution until all input data is
	     * available. It will also skip the action execution if all
	     * outputs are already marked ready. It is the responsibility
	     * of the actual action execution to mark outputs as ready.
	     */
	    public States action(String head,Data d) {
		Data.Element [] ins = null;
		Data.Element [] outs = null;

		// Synchronise with existence and readiness of inputs.
		if ( inputs != null ) {
		    ins = new Data.Element [ inputs.length ];
		    for ( int i = 0; i < inputs.length; i++ ) {
			ins[ i ] = d.find( inputs[ i ] );
			if ( ins[ i ] == null )
			    return States.BLOCKED;
			if ( ! ins[ i ].ready )
			    return States.BLOCKED;
		    }
		}

		if ( outputs != null ) {
		    outs = new Data.Element [ outputs.length ];
		    for ( int i = 0; i < outputs.length; i++ ) {
			outs[ i ] = d.create( outputs[ i ] );
		    }
		}

		// Invoke action implementation 
		Goal.States s = Action.this.execute( reentry, ins, outs );
		reentry = true;

		if ( s == Goal.States.PASSED ) {
		    // Mark all outputs as ready.
		    if ( outs != null ) {
			for ( int i = 0; i < outs.length; i++ ) {
			    outs[ i ].markReady();
			}
		    }
		}
		return s;
	    }
	}

	/**
	 * Makes a string representation of a string array.
	 */
	public String toString(String [] data)
	{
            StringBuffer s = new StringBuffer( "[ " );
	    if ( data != null ) {
		String sep = "";
		for ( int i = 0; i < data.length; i++ ) {
		    s.append( sep );
		    s.append( nameString( data[i] ) );
		    sep = ", ";
		}
	    }
	    s.append( " ]" );
	    return s.toString();
	}

	/**
	 * Makes a textual representation of this action's goal.
	 */
	public String toString(String counter) {
	    StringBuffer s = new StringBuffer( super.toString( counter ) );
	    s.append( toString( inputs ) );
	    s.append( " >> " );
	    s.append( toString( outputs ) );
	    //s.append( "\n" );
	    return s.toString();
	}
    }

    /**
     * A factory method that makes a "usage object" for this action,
     * representing its usage in a goal sentence. When performed,
     * the action's execute method is invoked with the actual in and out
     * Data.Element objects.
     */
    public Goal create(String [] ins,String [] outs)
    {
	return new Usage( ins, outs );
    }

    /**
     * The generic action performance method. Should be overridden by
     * extension class for action implementation.
     */
    public Goal.States execute(
	boolean reentry,Data.Element [] ins,Data.Element [] outs) {

	StringBuffer s = new StringBuffer( "Action " );
	s.append( Goal.nameString( name ) );
	s.append( " [" );
	String sep = "";
	if ( ins != null && ins.length > 0 ) {
	    for ( int i=0; i < ins.length; i++ ) {
		s.append( sep );
		sep = ", ";
		s.append( ins[i].toString() );
	    }
	}
	s.append( "] >> [" );
	sep = "";
	if ( outs != null && outs.length > 0 ) {
            for ( int i=0; i < outs.length; i++ ) {
		s.append( sep );
		sep = ", ";
		s.append( outs[i].toString() );
            }
	}
	s.append( "]" );
	System.err.println( s.toString() );
	return Goal.States.PASSED;
    }

    /**
     * Returns the {@link #group} attribute.
     */
    public String getTodoGroup() {
	return group;
    }

    /**
     * Assigns the {@link #group} attribute.
     */
    public void setTodoGroup(String v) {
	group = v;
    }

}
