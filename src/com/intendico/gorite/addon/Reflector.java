/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite.addon;

import com.intendico.gorite.Performer;
import com.intendico.gorite.Data;
import com.intendico.gorite.Goal;
import com.intendico.gorite.BDIGoal;
import com.intendico.data.Query;
import com.intendico.data.Ref;
import com.intendico.data.Snapshot;

import java.util.IdentityHashMap;
import java.util.Observer;
import java.util.Vector;
import java.util.Iterator;
import java.util.Observable;

/**
 * A Reflector is a reasoning faculty to translate situation changes
 * into percepts. The Reflector is populated with some number of
 * {@link Reflection} objects, which each combines a {@link Query}
 * with a {@link Goal}, so as to intend the {@link Goal} as a percept
 * when the {@link Query} changes state, from false to true or from
 * true to false, for particular bindings.
 *
 * <p> For example, consider the following code snippet:
 * <pre>
 *     new Reflector( performer, "grandparent", "inference" ) {{
 *         Ref p = new Ref( "parent" );
 *         addQuery( new And( parent.get( new Ref( "gp"), p ),
 *                            parent.get( p, new Ref( "ch") ) ) );
 *     }};
 * </pre>
 *
 * The example code would keep observing the "parent" relation, and
 * when it is updated to present a different grandparent-parent-child
 * linkage from before, the goal "grandparent" is intended on the
 * "inference" {@link com.intendico.gorite.Performer.TodoGroup
 * Performer.TodoGroup}, with the data elements "gp", "parent" and
 * "ch" set. Further, the data element "reflection event" is set to
 * "added" or "lost" to indicate whether the change is of adding or
 * losing a matching binding for the query.
 *
 *
 * <p> Technicallly each added {@link Query} gets wrapped into a
 * {@link Snapshot} in a {@link Reflector.Reflection} object, which is
 * set up as {@link Observer} to observe any changes to the {@link
 * Query} sources. The {@link Snapshot} wrapping acts as a filter to
 * ensure that only changes cause percepts. The percept will hold the
 * input query as "percept" data element, and all {@link Ref} object
 * values of the query as corresponding data elemens.
 *
 */
public class Reflector extends Perceptor {

    /**
     * The name of the data element to indicate whether its an
     * added or lost event
     */
    public String REFLECTION_EVENT = "reflection event";

    /**
     * Constructor that ties the Reflector to the given {@link
     * Performer}, causing goals as named in the {@link
     * com.intendico.gorite.Performer.TodoGroup Performer.TodoGroup}
     * as named.
     */
    public Reflector(Performer p,String goal,String todo) {
	super( p, goal, todo );
    }

    /**
     * Utility class for observing the query sources, and trigger
     * goals when bindings change state.
     */
    public class Reflection implements Observer {

	/**
	 * Holds the query wrapped into a {@link Snapshot}
	 */
	public Snapshot snap;

	/**
	 * Holds the perception goal caused by this reflection.
	 */
	public Goal goal;

	/**
	 * Holds the reflection mode; postive for reacting to added
	 * bindings, negative for reacting to lost bindings and zero
	 * for both.
	 */
	public int mode = 1;

	/**
	 * Alternative constructor, with a goal name.
	 */
	public Reflection(Query q,String g) {
	    this( q, g != null? new BDIGoal( g ) : null );
	}

	/**
	 * Constructor, which applies the {@link Snapshot} wrapping.
	 */
	public Reflection(Query q,Goal g) {
	    snap = new Snapshot( q );
	    snap.addObserver( this );
	    goal = g;
	}

	/**
	 * Implements {@link Observer#update} by reviewing the query
	 * and generating percepts for new bindings.
	 */
	synchronized public void update(Observable x,Object y) {
	    try {
		snap.reset();
		Vector/*<Ref>*/ refs = snap.getRefs( new Vector/*<Ref>*/() );
		if ( mode >= 0 ) {
		    // Process added bindings
		    while ( snap.next() ) {
			Data d = new Data();
			d.set( refs );
			d.setValue( REFLECTION_EVENT, "added" );
			perceive( snap.query, d, goal );
		    }
		}
		if ( mode <= 0 ) {
		    // Process lost bindings
		    for ( Iterator/*<Vector>*/ i =
			      snap.lost.iterator(); i.hasNext(); ) {
			Vector v = (Vector) i.next();
			Data d = new Data();
			Ref.bind( refs, v );
			d.set( refs );
			d.setValue( REFLECTION_EVENT, "lost" );
			perceive( snap.query, d, goal );
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

	/**
	 * Cancels the observation of query sources.
	 */
	public void cancel() {
	    snap.deleteObserver( this );
	}
    }

    /**
     * Keeps the current collection of queries, which are contained in
     * {@link Reflection} objects
     */
    public IdentityHashMap/*<Query,Reflection>*/ reflections =
        new IdentityHashMap/*<Query,Reflection>*/();

    /**
     * Adds a {@link Reflection} that uses the given {@link Query}.
     */
    public void addQuery(Query q) {
	addQuery( q, (String) null );
    }

    /**
     * Adds a {@link Reflection} query using the given {@link
     * Goal#name}.
     */
    public void addQuery(Query q,String goal) {
	addQuery( q, goal, 1 );
    }

    /**
     * Adds a {@link Reflection} query using the given {@link
     * Goal#name}.
     */
    public void addQuery(Query q,String goal,int m) {
	Reflection r = (Reflection) reflections.get( q );
	if ( r == null ) {
	    r = new Reflection( q, goal );
	    r.mode = m;
	    reflections.put( q, r );
	    r.update( null, null );
	} else if ( r.mode != m ) {
	    r.mode = 0;
	    r.update( null, null );
	}
    }

    /**
     * Adds a reflection query using the given Reflection goal.
     */
    public void addQuery(Query q,Goal goal) {
	addQuery( q, goal, 1 );
    }

    /**
     * Adds a reflection query using the given Reflection goal.
     */
    public void addQuery(Query q,Goal goal,int m) {
	Reflection r = (Reflection) reflections.get( q );
	if ( r == null ) {
	    r = new Reflection( q, goal );
	    r.mode = m;
	    reflections.put( q, r );
	    r.update( null, null );
	} else if ( r.mode != m ) {
	    r.mode = 0;
	    r.update( null, null );
	}
    }

    /**
     * Removes the {@link Reflection} that uses the given {@link
     * Query}.
     */
    public void removeQuery(Query q) {
	Reflection r = (Reflection) reflections.remove( q );
	if ( r != null ) {
	    r.cancel();
	}
    }

    /**
     * Returns a String representation of this object.
     */
    public String toString() {
	StringBuilder s = new StringBuilder();
	s.append( "Reflector(" + performer + "," +
		  handler + ",\"" + todo_group + "\"):" );
	for ( Iterator/*<Query>*/ ri = reflections.keySet().iterator();
	      ri.hasNext(); ) {
	    Query q = (Query) ri.next();
	    s.append( "\n  query: " );
	    s.append( q.toString() );
	}
	return s.toString();
    }
}
