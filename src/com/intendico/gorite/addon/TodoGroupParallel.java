/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite.addon;

import com.intendico.gorite.*;
import com.intendico.gorite.Performer.TodoGroup;
import java.util.Vector;

/**
 * This plan is a {@link TodoGroup} meta goal handler that combines
 * the "round-robin" with "skip blocked" management functions.
 * @see TodoGroupRoundRobin
 * @see TodoGroupSkipBlocked
 */
public class TodoGroupParallel extends Goal {

    /**
     * Constructor for providing {@link TodoGroup} management through
     * the named meta goal.
     */
    public TodoGroupParallel(String name) {
	super( name, new Goal [] {
		new TodoGroupRoundRobin( "round robin" ),
		new TodoGroupSkipBlocked( "skip blocked" )
	    } );
    }
}
