/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite.addon;

import com.intendico.gorite.*;
import com.intendico.gorite.Performer.TodoGroup;
import java.util.Vector;

/**
 * This plan is a {@link TodoGroup} meta goal handler to maintain
 * focus to "the first" running intention, i.e., to avoid that the
 * whole group is blocked when the top intention gets
 * blocked. Instead, when the top intention gets blocked, the
 * TodoGroup stack is scanned for its top-most non-blocked intention,
 * which then is promoted.  The actual goal name for the meta goal is
 * given at construction time, and unless it is the default {@link
 * Performer#TODOGROUP_META_GOAL}, the actual {@link TodoGroup}
 * instances to be managed by this goal need to be set up explicitly
 * using {@link Performer#addTodoGroup}.
 *
 * <p> Usage example:
 * <pre>
 * new Performer( "example" ) {
 *     addGoal( new TodoGroupSkipBlocked( "skip blocked todogroup meta goal" ) );
 *     addTodoGroup( "example", "skip blocked todogroup meta goal" );
 * }
 * </pre>
 *
 * <p> Another usage example, to make it the default todogroup meta
 * goal:
 * <pre>
 * new Performer( "example" ) {
 *     addGoal( new TodoGroupSkipBlocked( TODOGROUP_META_GOAL ) );
 * }
 * </pre>
 */
public class TodoGroupSkipBlocked extends Goal {

    /**
     * Constructor for providing {@link TodoGroup} management through
     * the named meta goal.
     */
    public TodoGroupSkipBlocked(String name) {
	super( name );
    }

    /**
     * Overrides {@link Goal#execute} to provide the meta goal
     * implementation. This expects a {@link Data} object with the
     * {@link TodoGroup} concerned as the element named by {@link
     * Performer#TODOGROUP}.  The implementation promotes the first
     * non-blocked intention, if any. Returns PASSED if there is a
     * non-blocked intention, an FAILED otherwise.
     */
    public States execute(Data d) {
	TodoGroup tg = (TodoGroup) d.getValue( Performer.TODOGROUP );
	Vector/*<Goal.Instance>*/ s = tg.stack;
	for ( int i = 0; i < s.size(); i++ ) {
	    if ( ((Instance) s.get( i )).state == States.STOPPED ) {
		tg.promote( i );
		return States.PASSED;
	    }
	}
	return States.FAILED;
    }

}
