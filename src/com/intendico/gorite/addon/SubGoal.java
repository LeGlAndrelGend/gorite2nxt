/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite.addon;

import com.intendico.gorite.*;

/**
 * This is a utility class for managing the dynamic state of a sub
 * goal execution within a (task) Goal's execute method. The execute
 * method that needs to perform sub goals (typically BDIGoal goals)
 * will need to obtain its SubGoal object on entry, and then dispatch
 * to the execution point appropriate for the SubGoal progress count.
 *
 * <p> Note that when performing sub goals, it is also appropriate to
 * re-implement the {@link Goal#cancelled} method for propagating
 * intention branch cancellation.
 *
 * <p> The following is an illustration of using SubGoal.
 * <pre>
 * static Goal goal1 = new BDIGoal( "the first sub goal" );
 * static Goal goal2 = new BDIGoal( "the second sub goal" );
 * static Goal goal3 = new BDIGoal( "the third sub goal" );
 *
 * addPlan( new Plan( "use SubGoal for something" ) {
 *
 *     // Propagate intention branch cancellation to sub goal, if any
 *     public void cancelled(Goal.Instance which) {
 *         SubGoal.cancel( which );
 *     }
 *
 *     // Plan body in Java, with sub goals:
 *     //
 *     // + Perform goal1 in state 0 with goal1;
 *     //
 *     // + Perform goal2 repeatedly through states 1-11, but break
 *     //   the loop and warp to state 12 on a LoopEndException;
 *     //   (with a trace output to stderr)
 *     //
 *     // + Perform goal3 in state 12, and swap its PASSED and FAILED
 *     //   result to be our result
 *
 *     public States execute(Data data) {
 *         SubGoal sub = SubGoal.get( data );
 *         if ( sub.inProgress( goal1, 0 ) )
 *             return sub.state();
 *         while ( sub.progress() < 12 ) {
 *             try {
 *                 if ( sub.inProgress( goal2 ) )
 *                     return sub.state();
 *             } catch (LoopEndException e) {
 *                 System.err.println( "END at state " + sub.progress() );
 *                 sub.count = 12;
 *                 break;
 *             }
 *         }
 *         boolean pass = false;
 *         if ( sub.inProgress( goal3, 12 ) ) {
 *             if ( sub.state() != FAILED ) {
 *                 return sub.state();
 *             }
 *             pass = true;
 *         }
 *         sub.done(); 
 *         return pass? PASSED : FAILED;
 *     }
 * } );
 * </pre>
 *
 * <p> Note that an intention branch may only have one active SubGoal
 * object at a time, and you have to go out of your way in coding to
 * juggle multiple concurrently active SubGoal objects. If you want
 * parallel execution, then you should execute an approprate
 * ParallelGoal rather than trying to implement your own parallel goal
 * execution management.
 *
 * <p> The sub goal execution may throw an exception, where it in
 * particular may be a {@link LoopEndException} or a {@link
 * ParallelEndException}. In most cases, the execute method would
 * ignore this, and thus allow them to be propagated up through the
 * invocation. In the example above, the goal2 invocation, being a
 * loop, catch {@link LoopEndException}, and let that break the loop
 * sucessfully.
 */
public class SubGoal {

    /**
     * This is a convenience field that an execution method may use
     * for marking its progress through its sub goal invocations. Its
     * value is automatically appended to the trace head for the next
     * sub goal, and it is incremented when a sub goal execution
     * terminates.
     * @see #progress()
     */
    public int count;

    /**
     * This is an internal cache of the {@link
     * com.intendico.gorite.Goal.Instance Goal.Instance} in progress,
     * if any. It is publically accessible, but the execution method
     * should refrain from changing it.
     * @see #inProgress(Goal,int)
     * @see #inProgress(Goal)
     */
    public Goal.Instance current;

    /**
     * This is an internal cache of the most recent return value of
     * the most recent sub goal invocation.  It is publically
     * accessible, but the execution method should refrain from
     * changing it. When the sub goal invocation is incomplete
     * (i.e. either BLOCKED or STOPPED), the execution method should
     * return this value.
     * @see #inProgress(Goal,int)
     * @see #inProgress(Goal)
     */
    public Goal.States result;

    /**
     * This is a handle to the Data object for the executing
     * intention.  It is publically accessible, but the execution
     * method should refrain from changing it.
     */
    public Data data;

    /**
     * Utility method that looks up a current SubGoal object in the
     * given Data, or creates a new one associated with the current
     * Data. To do so, it uses the Data element named by {@link
     * Data#thread_name}, which is unique within the intention for the
     * intention branch.
     */
    public static SubGoal get(Data data) {
	SubGoal sub = tryGet( data );
	return ( sub == null )? new SubGoal( data ) : sub;
    }

    /**
     * Utility method that looks up a current SubGoal object in the
     * given Data and returns it, or null, if none exists. You'd use
     * this in combination with a SubGoal extension class, e.g. for
     * carrying your own progress details. Thus, instead of
     * using {@link #get} to find the sub goal manager, you would have
     * a code snippet like the following:
     * <pre>
     * SubGoal sub = SubGoal.tryGet( data );
     * if ( sub == null ) {
     *     sub = new MySubGoal( data );
     * }
     * </pre>
     */
    public static SubGoal tryGet(Data data) {
	return (SubGoal) data.getValue( data.thread_name );
    }

    /**
     * Constructor. Normally SubGoal objects are only created
     * indirectly, either via the {@link #get} method, or via
     * <tt>super</tt> calls in extension classes. It always end up in
     * this constructor, which associates the newly created object
     * with the given {@link Data}, and puts itself as the Data
     * element named by the current {@link Data#thread_name}, which is
     * unique for the intention branch.
     */
    public SubGoal(Data d) {
	data = d;
	data.setValue( data.thread_name, this );
    }

    /**
     * Clean up by forgetting the data element named by the current
     * {@link Data#thread_name}.
     */
    public void done() {
	data.forget( data.thread_name );
    }

    /**
     * This method is used by the execution method to trigger and
     * pursue a sub goal execution. The method returns true if the
     * sub goal invocation returns without passing (i.e., returns
     * STOPPED, BLOCKED or FAILED), and it returns false when the sub
     * goal invocation returns PASSED. Also, when the sub goal returns
     * PASSED or FAILED, the count gets incremented before this method
     * returns.
     */
    public boolean inProgress(Goal goal)
	throws LoopEndException, ParallelEndException {
	if ( current == null ) {
	    String head = data.thread_name;
	    int i = head.lastIndexOf( "\"" );
	    if ( i > 0 )
		i = head.lastIndexOf( "\"", i - 1 );
	    i -= 1;
	    if ( i > 0 )
		head = head.substring( 0, i );
	    current = goal.instantiate( head + ":" + count, data );
	}
	result = current.perform( data );
	if ( result == Goal.States.PASSED || result == Goal.States.FAILED ) {
	    current = null;
	    count += 1;
	}
	return result != Goal.States.PASSED;
    }

    /**
     * This method verifies that the progress count is as given,
     * before pursuing sub goal execution as per {@link
     * #inProgress(Goal)}. Otherwise the method returns false;
     */
    public boolean inProgress(Goal goal,int at)
	throws LoopEndException, ParallelEndException {
	return count == at? inProgress( goal ) : false;
    }

    /**
     * Utility method to cancel any ongoing sub goal execution, and
     * force the count to a given state. If there is no sub goal in
     * progress, the method returns false. Otherwise, that sub goal
     * execution is cancelled and forgotten, {@link #result} is set to
     * CANCEL, and true is returned.
     */
    public boolean cancel(int next) {
	count = next;
	if ( current == null )
	    return false;
	current.cancel();
	current = null;
	result = Goal.States.CANCEL;
	return true;
    }

    /**
     * This method cancels and forgets any sub goal execution in
     * progress for the given {@link
     * com.intendico.gorite.Goal.Instance Goal.Instance}.
     */
    public static void cancel(Goal.Instance which) {
	Data data = which.data;
	String old = data.setThreadName( which.thread_name );
	SubGoal sub = (SubGoal) data.getValue( which.thread_name );
	if ( sub != null ) {
	    sub.cancel( sub.count + 1 );
	    sub.done();
	}
	data.setThreadName( old );
    }

    /**
     * Utility method that returns the current progress count.
     */
    public int progress() {
	return count;
    }

    /**
     * Utility method that returns the cached sub intention state.
     */
    public Goal.States state() {
	return result;
    }

    /**
     * Utility method that tells whether the current state has been
     * activated or not; i.e., whether the sub intention for the
     * current state has been created or not.
     */
    public boolean isActive() {
	return current != null;
    }

}
