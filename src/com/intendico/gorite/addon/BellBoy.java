/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite.addon;

import com.intendico.gorite.*;
import java.util.Hashtable;
import java.util.Set;

/**
 * This is a {@link Capability} (type) that provides a simple
 * inter-model messaging service within a single JVM. It uses the
 * {@link Performer#getName()} method of the containing {@link
 * Performer} as messaging end point identity. The idea is that all
 * communicating performers are set up to have their own instance of
 * this capability type. The following is an example of use, with a
 * receive plan outline:
 *
 * <pre>
 * new Performer( "morgan" ) {{
 *     addCapability( new BellBoy() );
 *     addPlan( new Plan( BellBoy.RECV, new Goal [] {
 *         .... deal with received message in "percept"
 *     } );
 * }};
 * </pre>
 *
 * <p><i>Sending Messages</i>
 *
 * <p> The capability handles a {@link #SEND "Send BellBoy message"}
 * {@link Goal goal}, with {@link #MSG "bellboy message"} holding a
 * {@link BellBoy.Envelope message envelope}, by delivering that
 * message to the {@link Envelope#getWhom() indicated recipient}.
 * 
 * <p> Sending can also be done directly in Java code by using the
 * static {@link BellBoy#send(Envelope)} or {@link
 * BellBoy#send(String,Object)} methods.
 *
 * <p><i>Receiving Messages</i>
 *
 * <p> Upon receiving a message, the capability triggers a {@link
 * #RECV "Got BellBoy message"} {@link BDIGoal} goal, on the {@link
 * #TODO "bellboy"} {@link com.intendico.gorite.Performer.TodoGroup
 * TodoGroup} and with {@link Perceptor#data_name "percept"} holding
 * the message,
 *
 * <p> Reception can also be triggered in Java code by using the
 * {@link #receive(Object)} method.
 *
 * <p> <b>NOTE</b>
 *
 * <p>Messages are delivered through in-core sharing, and they are not
 * copied! <b>Not copied!</b> This means that the sender and receiver
 * end up sharing the one message object. In proper use, the sender
 * should release all its handles to the message object after having
 * sent it. The capability supports this by {@link
 * Data#forget(String) forgetting} {@link #MSG "bellboy message"}
 * after having sent the message.
 */
public class BellBoy extends Capability {

    /**
     * The interface for standard BellBoy messages.
     */
    public interface Envelope {
	/**
	 * Returns message recipient identity.
	 */
	public String getWhom();

	/**
	 * Returns the message payload, i.e., the "actual" message.
	 */
	public Object getPayload();
    }

    /**
     * Convenience constant for the send goal.
     */
    public final static String SEND = "Send BellBoy message";

    /**
     * Convenience constant for the receive goal.
     */
    public final static String RECV = "Got BellBoy message";

    /**
     * Convenience constant for the send goal data element.
     */
    public final static String MSG = "bellboy message";

    /**
     * Convenience constant for BellBoy messaging {@link
     * com.intendico.gorite.Performer.TodoGroup TodoGroup}.
     */
    public final static String TODO = "bellboy";

    /**
     * The table of communicating BellBoy instances within this JVM.
     */
    public static Hashtable/*<String,BellBoy>*/ club =
	new Hashtable/*<String,BellBoy>*/();

    /**
     * The {@link Perceptor} for receiving BellBoy messages. This is
     * set up by the {@link #initialize()}, which is invoked
     * automatically by GORITE. The installed {@link Perceptor}
     * triggers {@link #RECV "Got BellBoy message"} goals on the
     * {@link #TODO "bellboy"} {@link
     * com.intendico.gorite.Performer.TodoGroup TodoGroup} with the
     * received messages as the {@link Perceptor#data_name "percept"}
     * data.
     */
    public Perceptor recv;

    /**
     * Binds a BellBoy to be a {@link #club} member under a given
     * name. This replaces any previous binding, and binding
     * <tt>null</tt> removes the previous binding.
     */
    public static void bind(String name,BellBoy b) {
	if ( b == null ) {
	    synchronized ( club ) {
		club.remove( name );
	    }
	} else {
	    synchronized ( club ) {
		club.put( name, b );
	    }
	}
    }

    /**
     * Returns the BellBoy bound to a given name.
     */
    public static BellBoy find(String name) {
	synchronized ( club ) {
            return (BellBoy) club.get( name );
	}
    }

    /**
     * Returns the names of the current {@link BellBoy#club} members.
     */
    public static Set/*<String>*/ getMembers() {
	synchronized ( club ) {
	    return club.keySet();
	}
    }

    /**
     * Creates a standard BellBoy {@link Envelope}.
     */
    public static Envelope pack(final String whom,final Object what) {
	return new Envelope() {
	    public String getWhom() {
		return whom;
	    }
	    public Object getPayload() {
		return what;
	    }
	};
    }

    /**
     * Receives a message. This causes a percept on the {@link #recv}
     * {@link Perceptor}. The method returns false if {@link #recv} is
     * unassigned, otherwise it returns true.
     */
    public boolean receive(Object m) {
	if ( recv == null )
	    return false;
	recv.perceive( m );
	return true;
    }

    /**
     * Sends a named recipient a message.
     */
    public static boolean send(String to,Object message) {
	BellBoy whom = find( to );
	return ( whom == null )? false : whom.receive( message );
    }

    /**
     * Sends an envelope.
     */
    public static boolean send(Envelope m) {
	return ( m == null )? false : send( m.getWhom(), m.getPayload() );
    }

    /**
     * Initializes the capability with the abilities to send and
     * receive messages, and then it joins the BellBoy club.
     *
     * <p> The capability is set up to handle a {@link #SEND "Send
     * BellBoy message"} {@link Goal goal}, with {@link #MSG "bellboy
     * message"} holding a {@link BellBoy.Envelope message envelope},
     * by delivering that message to the {@link Envelope#getWhom()
     * indicated recipient}.
     *
     * The goal fails if {@link #MSG "bellboy message"} is null. If
     * it's non-null but not a {@link BellBoy.Envelope}, then a {@link
     * ClassCastException} is thrown.
     *
     * The goal also fails if the {@link BellBoy#club} doesn't include
     * the desired recipient.
     */
    public void initialize() {
	// Prepare to receive messages
	recv = new Perceptor( getPerformer(), RECV, TODO );
	// Define ability to send message as goal
	addGoal( new Goal( SEND ) {
	    public States execute(Data data) {
		if ( send( (Envelope) data.getValue( MSG ) ) ) {
		    data.forget( MSG );
		    return States.PASSED;
		}
		return States.FAILED;
	    }
	} );
	// Join the club
	bind( getPerformer().getName(), this );
    }

}
