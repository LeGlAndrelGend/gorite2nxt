/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite.addon;

import com.intendico.gorite.Data;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class defines an Observable that schedules itself for issuing
 * a notfication to Observers at a future time.
 */
public class TimeTrigger extends Observable {

    /**
     * Utility method to establish a TimeTrigger as a {@link Data}
     * element value, and use as execution trigger after the given
     * delay. The indicated {@link com.intendico.gorite.Data.Element
     * Data.Element} should be unset initially, to be set by this
     * method to a TimeTrigger for the given delay. Upon subsequent
     * calls, the TimeTrigger is rescheduled until its deadline has
     * passed, at which time the given {@link
     * com.intendico.gorite.Data.Element Data.Element} is forgotten.
     * @return true if a trigger has been set and false otherwise
     */
    public static boolean isPending(Data data,String element,long delay) {
	TimeTrigger tt = (TimeTrigger) data.getValue( element );
	if ( tt == null ) {
	    tt = new TimeTrigger( delay );
	    data.setValue( element, tt );
	    data.setTrigger( tt );
	}
	if ( tt.reschedule() )
	    return true;
	data.forget( element );
	return false;
    }

    /**
     * A global {@link Timer} for scheduling future events on.
     */
    public static Timer timer = new Timer( true );

    /**
     * The deadline time for this TimeTrigger.
     */
    public long deadline;

    /**
     * Constructor for a given future time.
     */
    public TimeTrigger(long delay) {
	deadline = System.currentTimeMillis() + delay;
    }

    /**
     * Reset for triggering at a new future time.
     */
    public void reset(long delay) {
	deadline = System.currentTimeMillis() + delay;
    }

    /**
     * This method tests whether the trigger time is reached or not,
     * and if not, it sets up a {@link TimerTask} for notifying
     * observers when the trigger time is reached.
     */
    public boolean reschedule() {
	long delay = deadline - System.currentTimeMillis();
	if ( delay <= 0 )
	    return false;
	timer.schedule( new TimerTask() {
	    public void run() {
		setChanged();
		notifyObservers( this );
	    }
	}, delay );
	return true;
    }
}
