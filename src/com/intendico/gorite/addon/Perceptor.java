/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite.addon;

import com.intendico.gorite.*;

/**
 * This is a class for implementing a performer percept channel, where
 * each percept triggers an intention for handling it, which gets
 * added to a {@link com.intendico.gorite.Performer.TodoGroup} for
 * eventual processing.
 *
 * <p> The Perceptor class is made separate from Performer so as to
 * allow multiple, different perceptors and to simplify its use at the
 * perception source end.
 *
 * <p> A "pure" event driven system would be run via the {@link
 * Executor#run} method (rather than using a top level {@link
 * Performer#performGoal(Goal,String,Data)} call), and have Perceptor
 * objects to cause executions.
 */
public class Perceptor {

    /**
     * Keeps track of which {@link Performer} this is a perceptor
     * for. This is given as constructor argument, and it typically
     * remains unchanged. Though there is no harm in changing it and
     * thereby move the Perceptor to another Performer.
     */
    public Performer performer;

    /**
     * Keeps track of which {@link Goal} hierarchy to use for handling
     * a percept. It must not be null, and is given as optional
     * constructor argment. The default goal hierarchy is
     *
     * <pre>
     *     new BDIGoal( "handle percept" )
     * </pre>
     *
     * which thus allows the actual goal hierarchy be defined among
     * the performers capabilities, with possibly many alternative
     * handler plans depending on the kinds of percepts.
     */
    public Goal handler;

    /**
     * Keeps track of which {@link com.intendico.gorite.Data.Element}
     * name to use for the percept object. The default name is
     * "percept".
     */
    public String data_name = "percept";

    /**
     * Keeps track of which {@link
     * com.intendico.gorite.Performer.TodoGroup} to use for the
     * percept handling intentions. The default group is "percepts".
     */
    public String todo_group = "percepts";

    /**
     * Constructor with performer, goal and todo group name.
     */
    public Perceptor(Performer p,Goal g,String todo)
    {
	performer = p;
	handler = g;
	todo_group = todo;
	g.setGoalGroup( todo );
    }

    /**
     * Constructor with performer and goal, using default todo group.
     */
    public Perceptor(Performer p,Goal g)
    {
	performer = p;
	handler = g;
	g.setGoalGroup( todo_group );
    }

    /**
     * Alternative constructor with goal name and todo group name.
     */
    public Perceptor(Performer p,String goal,String todo) {
	this( p, new BDIGoal( goal ), todo );
    }

    /**
     * Alternative constructor with performer and name of a BDI goal
     * to be created here.
     */
    public Perceptor(Performer p,String goal)
    {
	this( p, new BDIGoal( goal ) );
    }

    /**
     * Alternative constructor with performer only, and using the
     * default percept handling goal <tt>"handle percept"</tt> and
     * default todo group <tt>"percepts"</tt>.
     */
    public Perceptor(Performer p)
    {
	this( p, "handle percept" );
    }

    /**
     * Utility method to reset the perceptor set up. The new settings
     * apply for subsequent perceive calls.
     */
    public void setup(String goal,String data,String todo) {
	handler = new BDIGoal( goal );
	handler.setGoalGroup( todo );
	data_name = data;
	todo_group = todo;
    }

    /**
     * The method to use for making the performer perceive
     * something. The given object is set up as value of a named data
     * element, and percept handling goal is instantiated for handling
     * it.
     */
    public void perceive(Object percept)
    {
	perceive( percept, new Data(), handler );
    }

    /**
     * Alternative perception method with a given a Data object.
     */
    public void perceive(Object percept,Data data) {
	perceive( percept, data, handler );
    }

    /**
     * Alternative perception method with a given a Data object and
     * special perception goal name.
     */
    public void perceive(Object percept,Data data,String goal) {
	perceive( percept, data, goal != null? new BDIGoal( goal ) : handler );
    }

    /**
     * Alternative perception method with a given a Data object and
     * special perception goal name.
     */
    public void perceive(Object percept,Data data,Goal goal) {

	if ( ! "X".equals( data.thread_name ) ) {
	    // This indicates that the Data object is already used or
	    // in use, which can cause all sorts of funny things
	    throw new Error( "Schizogenic Data object!!" );
	}

	data.setValue( data_name, percept );
	data.setValue( Goal.PERFORMER, performer );
	
	if ( goal == null )
	    goal = handler;
	Goal.Instance gi = goal.instantiate( todo_group, data );

	// The following set up is essential to make it appear that
	// the goal instace has been performed. (c.f {@link
	// Goal.Instance#perform})
	gi.data = data;
	gi.performer = performer;
	data.link( gi.thread_name );

	//System.err.println( "Data = " + data );
	//System.err.println( "Data:" + data_name + " = " + percept );

	try {
	    performer.execute( false, gi, todo_group);
	} catch (Exception e) {
	    // Not good :-(
	    e.printStackTrace();
	}
	performer.signalExecutor();
    }
}
