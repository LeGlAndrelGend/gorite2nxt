/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * The Goal class is base class for a goal oriented process modelling
 * through a hierarchy that reflects the decomposition of a goal as an
 * abstraction of sub goals.  A process model is a hierarchy with a
 * top level goal that is sucessively decomposed as particular
 * combinations of sub goals. The top level goal is achieved by means
 * of processing its sub goals according to the goal type.
 *
 * <p> A goal is processed by instantiating it via its {@link
 * #instantiate} method, which creates the {@link Instance} object
 * that represents an actual intention to achieve the goal. The
 * intention is then pursued via the {@link Instance#perform} method,
 * which eventually results in invoking one of the particular {@link
 * Instance#action} methods that implement the particular execution
 * semantics.
 * 
 * <p> The base class is also used for task goals, where the {@link
 * #execute} method is overridden to perform the intended task. In
 * many cases, the task execution is a matter of processing one or
 * more input data elements to proved one or more result data
 * elements, as illustrated by the following example:
 * <pre>
 * public States execute(Data data) {
 *     data.setValue( "result", process( data.getValue( "input" ) ) );
 *     return PASSED;
 * }
 * </pre>
 *
 * Of course, the data element names need to be designed and defined
 * in the full application context, since all goal executions of the
 * same intention use the same {@link Data} object. Typically you
 * would employ some data naming scheme to ensure appropriate name
 * separation.
 *
 * <p> Note that an Goal may be augmented with a reimplemented {@link
 * Goal#instantiate(String,Data)} method as way of defining data
 * values, a reimplemented {@link Goal#execute(Data,Goal.Instance)}
 * method as way of defining post processing, and a reimplemented
 * {@link Goal#cancelled(Goal.Instance)} method as way of restoring
 * state if the goal execution is cancelled.
 *
 * In particular, if the implementation requires a dynamic lexical
 * environment for each execution, then the {@link
 * Goal#instantiate(String,Data)} needs to create a vacuous Goal
 * extension with that environment and ultimately containing the
 * associated {@link Goal#execute(Data,Goal.Instance)} method. The
 * following is an illustration example of this.
 * <pre>
 * public Goal.Instance instantiate(String head,Data data) {
 *     return new Goal( "achieving " + getGoalName() ) {
 *         int x;
 *         public States execute(Data data) {
 *             // Sees a dynamically instantiated x
 *             while ( x++ < 10 ) {
 *                 return BLOCKED;
 *             }
 *             return PASSED;
 *         }
 *     }.instantiate( head, goal );
 * }
 * </pre>
 *
 * Using that approach, a vacuous Goal object, with the "int x"
 * member, is created for each instantation of the surrounding goal,
 * and then that goal is instantiated for providing the actual
 * Instance object.
 */
public class Goal {

    /**
     * Interface for intrusive tracing of goal execution.
     * @see #tracer
     */
    public interface Tracer {

	/**
	 * Invoked before performing an instance for given data. This
	 * method is invoked by the gorite executor thread prior to
	 * entering or re-entering the goal instance execution. A
	 * Tracer implementation may, by hogging the thread, hold back
	 * the execution while inspecting the model state. The Data is
	 * the instance data to be; this data object becomes the
	 * {@link Instance#data} upon the first entry (after the call
	 * to this method).
	 */
	public void enterPerform(Instance instance,Data d);

	/**
	 * Invoked after performing instance returns. This method is
	 * invoked when the execution thread stops performing the
	 * instance, which is either because it is finished, or
	 * because it is blocked or stopped. The given {@link States}
	 * is the execution return value, which in many cases is also
	 * cached in {@link Instance#state}.
	 */
	public void exitPerform(Instance instance,States s);

	/**
	 * Invoked after performing instance throws exception. This
	 * method is invoked when the execution thread stops
	 * performing the instance due to it throwing an exception or
	 * ({@link Throwable}).  If it happens to be a {@link
	 * LoopEndException} or {@link ParallelEndException}, then the
	 * exception is passed in on the call, otherwise null is
	 * passed in. Note that this invocation happens in the
	 * "finally block" for the pending exception propagation,
	 * which thus gets propagated when the method returns.
	 */
	public void exceptionPerform(Instance instance,Exception e);

    }

    /**
     * The global intrusive goal execution tracer, if any. This may be
     * set via the command line property <tt>-Dgorite.tracer=XX</tt>
     * where <tt>XX</tt> is the class name for the Tracer
     * implementation to instantiate. It may also be set in code.
     */
    public static Tracer tracer = null;

    /**
     * These are the return values of the {@link #execute(Data)}
     * method, to report on the state of a goal instance execution.
     */
    public /*enum*/ static class States {

	/**
	 * A "name" of a States object.
	 */
	protected String name;

	/**
	 * The PASSED state is returned to indicate that the goal
	 * execution has completed successfully.
	 */
	public final static States PASSED =
	    new States() {{ name = "PASSED"; }};

	/**
	 * The FAILED state is returned to indicate that the goal
	 * execution has completed without success.
	 */
	public final static States FAILED =
	    new States() {{ name = "FAILED"; }};

	/**
	 * The CANCEL state is not returned by goal instance
	 * execution, but is used to mark an {@link Instance} when the
	 * execution has been cancelled.
	 */
	public final static States CANCEL =
	    new States() {{ name = "CANCEL"; }};

	/**
	 * The BLOCKED state is returned to indicate that the goal
	 * execution has not completed, and that it cannot progress
	 * until a blocking condition has been removed. This return
	 * code is further a promise that something will notify the
	 * execution system via {@link Data#setFlag} call when the
	 * blocking condition possibly has changed. Having returned
	 * this code, the goal instance execution may be re-entered
	 * whenever the execution system so wishes, regardless of the
	 * blocking condition, and the execution method needs to
	 * confirm the condition or return BLOCKED again.
	 * @see Data#setTrigger(Observable)
	 * @see Data#setTrigger(Query)
	 */
	public final static States BLOCKED =
	    new States() {{ name = "BLOCKED"; }};

	/**
	 * The STOPPED state is returned to indicate that the goal
	 * execution has not completed, but is interrupted for some
	 * reason other than a blocking condition. The goal instance
	 * execution is thus expecting re-entry as soon as possible.
	 */
	public final static States STOPPED =
	    new States() {{ name = "STOPPED"; }};

	/**
	 * Returns the name.
	 */
	public String toString() {
	    return name;
	}
    }

    /**
     * The property name for tracing goal execution.
     */
    public static final String TRACE = "gorite.goal.trace";

    /**
     * The name of the data element that identifies the executing
     * performer.
     */
    public static final String PERFORMER = "this performer";

    /**
     * The name of this goal. There are no restrictions on which names
     * goals have, but it is the name that identifies the goal when
     * resolving a {@link BDIGoal} or a {@link TeamGoal} into some
     * plan to achieve it. Conceptually it is the name that *is* the
     * goal, wheras a Goal object rather is a node in a goal hierarchy
     * to represent the idea of achieving the named goal.
     *
     * <p> An actual attempt to achieve the named goal is also
     * separate both from the goal (name) itself, and from the Goal
     * object (the idea of achieving it). Such an attempt is
     * understood as an "intention", which in GORITE gets represented
     * by a {@link Goal.Instance}.
     */
    private String name = null;

    /**
     * The control data, if any. This is only used by certain kinds of
     * goals, to carry some certain definition detail separate from
     * the goal type.
     *
     * <p> It is for instance used by {@link ParallelGoal} and {@link
     * RepeatGoal} goals to name the data element that identifies each
     * branch.
     * @see Data#fork(String,int,String)
     * @see Data#join(String,HashSet,String)
     */
    private String control = null;

    /**
     * The execution group to be used for pursuing the goal. If set,
     * it is the name of the {@link Performer.TodoGroup} to use for
     * goal instance execution. Goals of the same group are then
     * managed by the execution management associated with the {@link
     * Performer.TodoGroup}.
     */
    private String group = null;

    /**
     * The sub goals of this goal.
     */
    private Goal [] subgoals;

    /**
     * Where is this created?
     */
    final public StackTraceElement created;

    /**
     * Constructor.
     */
    public Goal(String n,Goal [] sg) {
	name = n;
	subgoals = sg;
	StackTraceElement [] where = new Error().getStackTrace();
	for ( int i = 0; i < where.length; i++ ) {
	    String c = where[i].getClassName();
	    if ( ! c.startsWith( "com.intendico.gorite." ) ) {
		created = where[i];
		return;
	    }
	}
	created = null;
    }

    /**
     * Convenience constructor without sub goals. The sub goals may be
     * set explicitly as value to the {@link #subgoals} member.
     */
    public Goal(String n) {
	this( n, null );
    }

    /**
     * This flag captures whether or not the {@link #TRACE} property
     * set (to any value) upon class initialisation, and it may be set
     * or reset subsequently for controlling the amount of goal
     * execution logging to generate.
     * 
     * <p> Ordinarily one will turn on standard goal execution tracing
     * by using the command line argument:
     * <pre>
     * -Dgorite.goal.trace=yes
     * </pre>
     */
    public static boolean tracing = System.getProperty( TRACE, null ) != null;

    // Set up a tracer if declared
    static {
	String c = System.getProperty( "gorite.tracer", null );
	if ( c != null ) {
	    try {
		tracer = (Tracer) Class.forName( c ).newInstance();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }
    /**
     * This is a utility method to discover whether tracing has been
     * requested by defining the {@link #TRACE} property, or not.
     */
    public static boolean isTracing() {
	return tracing;
    }

    //
    // Execution support
    //

    /**
     * This is the primary execution method for a goal. It is invoked
     * from the {@link Instance} object for a goal so as to achieve
     * the goal, and it is told about both the calling goal {@link
     * Instance} and {@link Data}. This base implementation invokes
     * {@link Instance#action(String,Data)} for actual progress of the
     * intention.
     *
     * <p> This method may be overridden by a user method when
     * defining a task goal that needs access to the {@link Instance}
     * object. See {@link #execute(Data)} for details.
     */
    public States execute(Data d,Instance i)
        throws LoopEndException, ParallelEndException {
        return i.action( i.head, d );
    }

    /**
     * This is the simplified execution method for task goal. It is
     * invoked from the {@link Instance} object for a task goal so as
     * to achieve the goal by means of Java code, and it is told only
     * about the intention {@link Data}. This base implementation
     * merely returns {@link States#PASSED}, and it should be
     * overridden by a user method when defining an actual task goal.
     *
     * <p> The method returns either {@link States#PASSED} or {@link
     * States#FAILED} to indicate that the execution has terminated
     * and the goal is achieved or not, or it returns {@link
     * States#STOPPED} or {@link States#BLOCKED} to indicate that the
     * attempt of achieving the goal is still going on, but execution
     * control is relinquished temporarily (so as to allow other
     * intentions to progress). By returning {@link States#BLOCKED},
     * it also indicates that this code need not be invoked unless
     * some other intention has progressed, but the {@link Executor}
     * may choose to go idle. This is typically combined with the
     * setting of an execution trigger, which is done via {@link
     * Data#setTrigger(Query)} or {@link Data#setTrigger(Observable)},
     * to ensure that the {@link Executor} wakes up when something
     * interesting has happened.
     */
    public States execute(Data d)
	throws InterruptedException, LoopEndException, ParallelEndException {
	return States.PASSED;
    }

    /**
     * Utility method for instantiating a goal according to its
     * type. Each goal type has its own instance class extension,
     * which implement the ways in which a goal is achieved through
     * its sub goals.
     */
    public Instance instantiate(String head,Data d) {
	return new Instance( head );
    }

    /**
     * Control callback whereby the goal gets notified that an (its)
     * intention is cancelled.
     */
    public void cancelled(Instance which) {
    }

    /**
     * Base class for goal instance execution procedures.
     */
    public class Instance {

	/**
	 * The trace name of the execution point. This defines where
	 * in the abstract goal execution tree this instance appears.
	 * The head is a kind of cursor into the abstract tree, set up
	 * as a sequence of numbers that tell which branch to follow
	 * so as to eventually arrive at this node. Branches are
	 * numbered from 0 and up, i.e. branch #3 is the fourth
	 * branch; for some goal types the branch number is also an
	 * index into the {@link #subgoals} array for the goal the
	 * execution is an instantiation of.
	 * The numbers are separated by characters that mark which
	 * kind of expansion the abstract tree has at the point.
	 * <ul>
	 * <li> ".n" marks insantiation of subgoal n
	 * <li> ":n" marks repetition n or branch n, for {@link
	 * LoopGoal}, {@link ParallelGoal}, {@link RepeatGoal} and
	 * {@link TeamGoal}.
	 * <li> "*n" marks attempt n for a {@link BDIGoal}
	 * </ul>
	 */
	final public String head;

	/**
	 * A more verbose intention name, which consists of the head
	 * and the goal name. This is used as key for the {@link
	 * Data.Bindings} of this intention.
	 */
	final public String thread_name;

	/**
	 * The Data object for this instance.
	 */
	public Data data = null;

	/**
	 * The monitoring state. This is the state an intention
	 * monitor should return; it starts off as BLOCKED, and turns
	 * into PASSED, FAILED or STOPPED (upon exception).
	 */
	public States state = States.BLOCKED;

	/**
	 * Returns the goal of this instance.
	 */
	public Goal getGoal() {
	    return Goal.this;
	}

	/**
	 * Returns the plan argument object, if any, for this
	 * instance.
	 */
	public Object getArgs() {
	    return data.getArgs( head );
	}

	/**
	 * Determine the plan-local variable name for the given short
	 * name.
	 */
	public String local(String name) {
	    int i = head.lastIndexOf( "*" );
	    if ( i < 0 )
		i = head.length();
	    return head.substring( 0, i ) + "-" + name;
	}

	/**
	 * Returns the value of a data element as seen by this
	 * intention.
	 */
	public Object getValue(String name) {
	    if ( data == null )
		return null;
	    return data.getValue( name, thread_name );
	}

	/**
	 * Sets a data value onto the innermost lookup path as seen by
	 * this intention. If an element of the name is found, then
	 * that element gains a new value, otherwise a new element is
	 * created for the most local binding of this intention.
	 */
	public void setValue(String name,Object value) {
	    data.setValue( thread_name, name, value );
	}

	/**
	 * Utility method for updating the monitoring state, which
	 * also notifies any waiting thread.
	 */
	synchronized public void setMonitoring(States s) {
	    if ( isTracing() )
		trace( "-- noting", " (" + s + ")" );
	    state = s;
	    if ( state == States.BLOCKED )
		state = s;
	    else
		data.setFlag();
	}

	/**
	 * Utility method to cancel the execution of this instance.
	 * This method is typically overridden by extension classes in
	 * order to propagate cancellation.
	 */
	public void cancel() {
	    if ( isTracing() )
		System.err.println( "** cancel " + head + " " + this );
	    if ( state == States.BLOCKED || state == States.STOPPED )
		state = States.CANCEL;
	    // Notify Goal about cancelling this intention.
	    cancelled( this );
	    if ( data != null )
		data.dropBindings( thread_name );
	}

	/**
	 * Utility thread control method to wait until this instance
	 * is done.
	 */
	synchronized public States waitDone()
	    throws LoopEndException, ParallelEndException {
	    if ( state == States.BLOCKED ) {
		trace( "-- waiting on ", "" );
		data.waitFlag();
	    }
	    throwSavedException();
	    return state;
	}

	/**
	 * Utility funtion that throws the particular exception, if
	 * any, that has been saved for this instance, or just
	 * returns.
	 */
	public void throwSavedException()
	    throws LoopEndException, ParallelEndException {
	    if ( exception instanceof LoopEndException )
		throw (LoopEndException) exception;
	    if ( exception instanceof ParallelEndException )
		throw (ParallelEndException) exception;
	}
	    
	/**
	 * Keeps any LoopEndException or ParallelEndException thrown
	 * by a sub goal.
	 */
	public Exception exception = null;

	/**
	 * The instance performer.
	 */
	public Performer performer = null;

	/**
	 * Constructor, taking the execution point name as argument.
	 */
	public Instance(String h) {
	    head = h.replace( '\"', ':' );
	    thread_name = head + " " + nameString( name );
	}

	/**
	 * Utility method to issue a standard trace message.
	 */
	public void trace(String prefix,String suffix) {
	    if ( isTracing() ) {
		System.err.println(
		    prefix + " " +
		    ( performer == null? "" : ( performer.name + ":" ) ) +
		    thread_name + suffix );
	    }
	}

	/**
	 * This is the primary entry point for executing this goal
	 * instance. It is invoked repeatedly by the calling goal
	 * instance for the purpose of progressing this goal instance
	 * via its {@link Goal#execute(Data,Instance)} method. On the
	 * first invokation, which is detected by virtue of {@link
	 * #data} being unset, this method registers the instance to
	 * the intention data and sets up the {@link #PERFORMER} data
	 * element.
	 *
	 * <p> This method also invokes {@link
	 * Performer#propagateBeliefs()} before progressing the
	 * intention.
	 *
	 * <p> If the enclosing goal's {@link Goal#group} attribute is
	 * non-null, then it is understood to name a {@link
	 * Performer.TodoGroup} for coordinating instances of this
	 * goal. Therefore, instead of actually progressing the
	 * intention, this instance gets added to the named {@link
	 * Performer.TodoGroup} (by {@link Performer#execute}), and
	 * subsequently all progress will occur "in parallel" via
	 * calls to the {@link #action()} method, while this method
	 * henceforth results in merely polling the instance state
	 * (which happens indirectly via the subsequent calls to
	 * {@link Performer#execute}).
	 */
	public States perform(Data d)
	    throws LoopEndException, ParallelEndException {
	    Tracer t = tracer;
	    if ( t == null ) {
		try {
		    return performIt( d );
		} catch(LoopEndException x) {
		    throw x;
		} catch(ParallelEndException x) {
		    throw x;
		} catch (Throwable x) {
		    throw new GoriteError( this, x );
		}
	    }
	    t.enterPerform( this, d );
	    States b = States.STOPPED;
	    boolean returns = false;
	    Exception e = null;
	    try {
		b = performIt( d );
		returns = true;
		return b;
	    } catch (LoopEndException x) {
		e = x;
		throw x;
	    } catch (ParallelEndException x) {
		e = x;
		throw x;
	    } catch (Throwable x) {
		throw new GoriteError( this, x );
	    } finally {
		if ( returns ) {
		    t.exitPerform( this, b );
		} else {
		    t.exceptionPerform( this, e );
		}
	    }
	}
	
	/**
	 * The working method for performing this Instance. It used to
	 * be {@link #perform}, but nowadays that method only deals
	 * with the {@link #tracer}, while invoking this method for
	 * the actual work.
	 * @see #perform
	 */
	private States performIt(Data d)
	    throws LoopEndException, ParallelEndException {
	    try {
		if ( state == States.CANCEL ) {
		    trace( "--", " (cancel)" );
		    cancel();
		    return States.CANCEL;
		}

		boolean reentry = true;
		if ( data == null ) {
		    trace( "=>", " (group=" + group + ")" );
		    data = d;
		    data.link( thread_name );
		    reentry = false;
		    performer = (Performer) data.getValue( PERFORMER );
		    if ( performer == null ) {
			System.err.println(
			    "** Missing performer for goal \"" +
			    name + "\" at " + head );
			System.err.println( "---------------------\n" + data );
			//System.exit( 1 );
		    }
		} else {
		    if ( state == States.BLOCKED &&
			 ! data.isRunning( thread_name ) )
			return States.BLOCKED;
		    trace( "=>", " (resume)" );
		}

		if ( ! reentry && performer.changeFocus() ) {
		    if ( group == null )
			return States.STOPPED ;
		}

		performer.propagateBeliefs();

		States b;
		if ( group == null ) {

		    b = execute( data, this );
		    if ( b == States.PASSED || b == States.FAILED ) {
			data.dropBindings( thread_name );
		    }
		} else {
		    b = performer.execute( reentry, this, group );
		}
		setMonitoring( b );
		
		if ( isTracing() ) {
		    trace( "<=", " (" + b + ")" );
		}

		return b;
	    } catch (LoopEndException e) {
		if ( data != null )
		    data.dropBindings( thread_name );
		throw e;
	    } catch (ParallelEndException e) {
		if ( data != null )
		    data.dropBindings( thread_name );
		throw e;
	    }
	}

	/**
	 * Utility method that runs this instance's
	 * action(String,Data). This is used for resuming the instance
	 * from a {@link Performer.TodoGroup}.
	 */
	public States action() {
	    if ( state == States.CANCEL ) {
		trace( "--*", " (cancel)" );
		cancel();
		return States.CANCEL;
	    }
	    state = States.BLOCKED;
	    try {
		data.setThreadName( thread_name );
		if ( performer == null ) {
		    throw new Error(
			"** Missing performer for goal \"" +
			name + "\" at " + head );
		}
		data.replaceValue( Goal.PERFORMER, performer );
		trace( "=>*", " (resume)" );
		performer.propagateBeliefs();
		//return action( head, data );
		return execute( data, this );
	    } catch (LoopEndException e) {
		exception = e;
		return States.STOPPED;
	    } catch (ParallelEndException e) {
		exception = e;
		return States.STOPPED;
	    }
	}

	/**
	 * Utility method to access a data element relative to this
	 * thread.
	 */
	public Data.Element getDataElement(String name) {
	    return data.getLocalElement( thread_name, name );
	}

	/**
	 * Implements how a goal is achieved by means of its sub
	 * goals.  This base implementation invokes the goal's {@link
	 * #execute(Data)}.
	 */
	public States action(String head,Data d)
	    throws LoopEndException, ParallelEndException {
	    try {
		return execute( d );
	    } catch (InterruptedException e) {
		e.printStackTrace();
		return States.STOPPED;
	    }
	}
    }

    /**
     * Utility method to quote a string.
     */
    public static String nameString(String n)
    {
	if ( n == null )
	    return "null";
	return "\"" + n + "\"";
    }

    /**
     * Returns a {@link java.lang.String} identifying the type of the
     * goal.
     */
    public String getType()
    {
	String s = getClass().getName();
	int i = s.lastIndexOf( "." );
	return s.substring( i + 1 );
    }

    /**
     * Makes a deep-structure string representation of this goal and
     * all its sub goals, using a given depth prefix. It does not
     * check for cyclic goal structure.
     */
    public String toString(String counter) {
	StringBuffer s = new StringBuffer();
	String x = "";
	if ( counter != null ) {
	    s.append( counter );
	    s.append( " " );
	    x = counter + ".";
	}
	s.append( nameString( name ) );
	s.append( " (" );
	s.append( getType() );
	s.append( ")" );
	if ( subgoals != null ) {
	    for ( int i = 0; i < subgoals.length; i++ ) {
		s.append( "\n" );
		s.append( subgoals[ i ].toString( x + (i+1) ) );
	    }
	    //s.append( " // end " + counter + "\n" );
	}
	return s.toString();
    }

    /**
     * Makes a deep-structure string representation of this goal and
     * all its sub goals.
     */
    public String toString() {
	return toString( null );
    }

    /**
     * Returns the goal {@link #name} attribute.
     */
    public String getGoalName() {
	return name;
    }

    /**
     * Returns the goal {@link #control} attribute.
     */
    public String getGoalControl() {
	return control;
    }

    /**
     * Returns the {@link #subgoals} attribute.
     */
    public Goal [] getGoalSubgoals() {
	return subgoals;
    }

    /**
     * Returns the {@link #group} attribute.
     */
    public String getGoalGroup() {
	return group;
    }

    /**
     * Assigns the {@link #control} attribute.
     */
    public void setGoalControl(String v) {
	control = v;
    }

    /**
     * Assigns the {@link #subgoals} attribute.
     */
    public void setGoalSubgoals(Goal [] s) {
	subgoals = s;
    }

    /**
     * Assigns the {@link #group} attribute.
     */
    public void setGoalGroup(String v) {
	group = v;
    }
}
