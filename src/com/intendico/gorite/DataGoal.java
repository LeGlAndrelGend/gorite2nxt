/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

/**
 * A DataGoal is a goal that uses the value of the data element named
 * by the control attribute as the actual goal to achieve.  This value
 * is accessed and considered whenever the DataGoal is instantiated
 * (via {@link #instantiate(String,Data)}), where it either is a
 * {@link Goal}, a {@link Goal.Instance}, or some other non-null
 * object.
 * <ul>
 *
 * <li> If the value is null, the DataGoal will instantiate as a
 * FailGoal, and thus fail when executed.
 *
 * <li> A Goal is instantiated and executed in lieu of the DataGoal,
 * and likewise for a Goal.Instance, which is used as given and
 * executed in lieu of the DataGoal.
 *
 * Note that a Goal.Instance execution may then a different Data
 * object than for the DataGoal instantiation, and if so, there is no
 * support for data transfer between the Data objects. Further, the
 * Goal.Instance execution progresses from whatever execution state it
 * has.
 *
 * <li> For any other type of object, the DataGoal creates a vacuous
 * BDIGoal for the object, and that BDIGoal is instantiated and
 * executed in lieu of the DataGoal.
 * </ul>
 *
 * <p> Except for Goal.Instance values (which are already
 * instantiated), the DataGoal goal adds the suffix "[xx]", where xx
 * is the control data name, to the intention head, as its marker in
 * an execution trace output.
 */
public class DataGoal extends Goal {

    /**
     * Constructor with goal name and control data name.
     */
    public DataGoal(String n,String c) {
	super( n );
	setGoalControl( c );
    }

    /**
     * Overrides the base class method {@link Goal#instantiate} to
     * provide the instance for the data goal.
     */
    public Instance instantiate(String h,Data d) {
	Object g = d.getValue( getGoalControl() );
	if ( g instanceof Instance ) {
	    return (Instance) g;
	}
	String hh = h + "[" + getGoalControl() + "]";
	if ( g instanceof Goal ) {
	    return ((Goal) g).instantiate( hh, d );
	}
	if ( g != null ) {
	    return new BDIGoal( g ).instantiate( hh, d );
	}
	return new FailGoal( "Data getGoalControl() is null" ).
	    instantiate( hh, d );
    }
}
