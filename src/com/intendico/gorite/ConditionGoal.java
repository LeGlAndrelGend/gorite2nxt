/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * A ConditionGoal is achieved by achieving any sub goal, which are
 * attempted in sequence. The goal fails when and if all sub goals
 * have been attempted and failed.
 */
public class ConditionGoal extends Goal {

    /**
     * Constructor.
     */
    public ConditionGoal(String n,Goal [] sg) {
	super( n, sg );
    }

    /**
     * Convenience constructor without sub goals.
     */
    public ConditionGoal(String n) {
	this( n, null );
    }

    /**
     * Creates and returns an instance object for achieving
     * a ConditionGoal.
     */
    public Instance instantiate(String head,Data d) {
	return new ConditionInstance( head );
    }

    /**
     * Implements sequential, conditional sub goal execution
     * attempt. I.e., if a sub goal fails, then try next, otherwise
     * succeed.
     */
    public class ConditionInstance extends Instance {

	/**
	 * Constructor.
	 */
	public ConditionInstance(String h) {
	    super( h );
	}

	/**
	 * Next sub goal index.
	 */
	public int index = 0;

	/**
	 * Current ongoing Instance.
	 */
	public Instance ongoing = null;

	/**
	 * Cancels this execution by propagating the cancel call to
	 * all sub goals.
	 */
	public void cancel() {
	    super.cancel();
	    if ( ongoing != null )
		ongoing.cancel();
	}

	/**
	 * Instantiates and performs sub goals in sequence, until the
	 * first one that does not fail. If a sub goal fails, then
	 * that is caught, and the next sub goal in sequence is
	 * instantiated and performed.
	 */
	public States action(String head,Data d)
            throws LoopEndException, ParallelEndException {
	    Goal [] subgoals = getGoalSubgoals();
	    if ( subgoals == null )
		return States.FAILED;
	    while ( index < subgoals.length ) {
		if ( ongoing == null ) {
		    ongoing = subgoals[ index ].instantiate(
			head + "." + index, d );
		}
		States s = ongoing.perform( d );
		if ( s != States.FAILED )
		    return s;
		index += 1;
		ongoing = null;
	    }
	    return States.FAILED;
	}
    }
}
