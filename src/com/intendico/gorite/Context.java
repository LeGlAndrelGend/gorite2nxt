/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;
import com.intendico.data.Condition;

/**
 * The Context interface is implemented by a plan (i.e. top level
 * goal) that has different variants for different contexts. The
 * different contexts are defined by means of a {@link Query} that
 * provides multiple bindings, and each binding defines a variant
 * execution of the plan.
 * @see Plan
 * @see BDIGoal
 */

public interface Context {

    /**
     * A constant that a {@link #context} method may return when a
     * plan doesn't have any applicable variants. Returning this
     * {@link Query}, which is eternally false, has the same effect as
     * throwing a {@link None} exception in marking that the plan does
     * not have any applicable variants.  The point is that a null
     * return from the {@link #context} method means that there is no
     * context query, and that the goal with this context is
     * applicable once.
     */
    public static final Query EMPTY = new Condition() {
	    public boolean condition() {
		return false;
	    }
	};

    /**
     * A constant that a {@link #context} method may return when a
     * plan has a single applicable variant without bindings to {@link
     * Ref} objects. Returning this {@link Query}, which in fact is
     * null, has the same effect as returning null in marking that the
     * plan has a single applicable variant.  The point is that a null
     * return from the {@link #context} method means that there is no
     * context query, and therefore the plan with this context is
     * applicable.
     */
    public static final Query TRUE = null;

    /**
     * A constant that a {@link #context} method may return when a
     * plan doesn't have any applicable variants. This is merely an
     * alias for {@link #EMPTY} added for convenience.
     */
    public static final Query FALSE = EMPTY;

    /**
     * An exception that a {@link #context} method may throw in
     * order to mark that there is no applicable context.
     * @see #EMPTY
     */
    public static class None extends Exception {

	/**
	 * A default constructor.
	 */
	public None() {
	    super( "no applicable context" );
	}

	/**
	 * Version identity required for serialization.
	 */
	public static final long serialVersionUID = 1L;
    }

    /**
     * The context method returns the {@link Query} that defines the
     * multiple alternative contexts for the execution.  The {@link
     * #context} method is invoked for defining the {@link Query}. The
     * method may return null, which means that the plan has a single
     * applicable variant, or it may return {@link #EMPTY} or throw
     * the {@link None} excpetion, which both indicate that the plan
     * does not have any applicable variant (right now). Any other
     * query is taken as defining the alternative applicable contexts.
     * @see BDIGoal.BDIInstance#contextual
     */
    public Query context(Data d) throws Exception;


}
