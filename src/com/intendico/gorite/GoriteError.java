package com.intendico.gorite;

/**
 * This is a specialozed {@link Error} that used for wrapping
 * exceptions in a gorite intention chain, so as to give better
 * guidance about where in the source execution the error occurs.
 */
public class GoriteError extends Error {

    /**
     * Constructor.
     */
    public GoriteError(Goal.Instance instance,Throwable cause) {
	super( "Uncaught Throwable" );
	if ( cause instanceof GoriteError ) {
	    cause = cause.getCause();
	}

	// Reduce the stack trace for the cause in a good way...
	StackTraceElement [] stack = getStackTrace();
	StackTraceElement [] inner = cause.getStackTrace();
	
	int at = inner.length - stack.length;
	StackTraceElement created = instance.getGoal().created;
	inner[ at ] = new StackTraceElement(
	    created.getClassName(),
	    created.getMethodName() +
	    "|" + instance.getGoal().getGoalName() + "|" + instance.head,
	    created.getFileName(),
	    created.getLineNumber() );
	cause.setStackTrace( inner );
	initCause( cause );
    }

}
