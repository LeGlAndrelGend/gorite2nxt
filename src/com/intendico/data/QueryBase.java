/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Observer;
import java.util.Vector;

/**
 * The QueryBase class is a wrapper class that provides transparent
 * implementations for the {@link Query} methods. It is intended to be
 * base class for simple query implementations that only need to
 * override one or a few of the implementations, typically the
 * constructor.
 */
public class QueryBase implements Query {

    /**
     * Holds a/the sub query wrapped by this QueryBase.
     */
    public Query query;

    /**
     * Constructor;
     */
    public QueryBase() {
    }

    /**
     * Constructor;
     */
    public QueryBase(Query q) {
	query = q;
    }

    /**
     * The {@link Query#copy} method implemented by returning this
     * same query.
     */
    public Query copy(Vector/*<Ref>*/ newrefs) throws Exception {
	return this;
    }

    /**
     * The {@link Query#reset} method implemented by forwarding to the
     * wrapped query.
     */
    public void reset() throws Exception {
	query.reset();
    }

    /**
     * The {@link Query#next} method implemented by forwarding to the
     * wrapped query.
     */
    public boolean next() throws Exception {
	return query.next();
    }

    /**
     * The {@link Query#getRefs} method implemented by forwarding to the
     * wrapped query.
     */
    public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v) {
	return query.getRefs( v );
    }

    /**
     * The {@link Query#addObserver} method implemented by forwarding
     * to the wrapped query.
     */
    public void addObserver(Observer x) {
	query.addObserver( x );
    }

    /**
     * The {@link Query#deleteObserver} method implemented by
     * forwarding to the wrapped query.
     */
    public void deleteObserver(Observer x) {
	query.deleteObserver( x );
    }

    /**
     * The {@link Query#addable} method implemented by forwarding to
     * the wrapped query.
     */
    public boolean addable() {
	return query.addable();
    }

    /**
     * The {@link Query#add} method implemented by forwarding to the
     * wrapped query.
     */
    public boolean add() {
	return query.add();
    }

    /**
     * The {@link Query#removable} method implemented by forwarding to
     * the wrapped query.
     */
    public boolean removable() {
	return query.removable();
    }

    /**
     * The {@link Query#remove} method implemented by forwarding to
     * the wrapped query.
     */
    public boolean remove() {
	return query.remove();
    }

    /**
     * Returns the textual representation of the wrapped query, within
     * parentheses.
     */
    public String toString() {
	return "(" + query + ")";
    }
}
