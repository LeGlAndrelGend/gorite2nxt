/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data.addon;

import com.intendico.data.*;
import com.intendico.gorite.Data;
import com.intendico.sdp.BNFGrammar;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

/**
 * This class defines the textual predicate language. The detailed
 * syntax is as follows:
 *
 * <pre>
 * rule ::= &lt;predicate&gt; '=&gt; &lt;predicate&gt;
 *
 * predicate ::= 'And '( &lt;predicates&gt; ')
 *   | 'Or '( &lt;predicates&gt; ')
 *   | 'Not &lt;predicate&gt;
 *   | 'Lost &lt;predicate&gt;
 *   | 'Equals '( &lt;arguments&gt; ')
 *   | 'Distinct '( &lt;arguments&gt; ')
 *   | &lt;name&gt; '( &lt;arguments&gt; ')
 *
 * predicates ::= &lt;predicate&gt; ', &lt;predicates&gt;
 *   | &lt;predicate&gt;
 *
 * arguments ::= &lt;argument&gt; ', &lt;arguments&gt;
 *   | &lt;argument&gt;
 *
 * argument ::= &lt;number&gt;
 *   | &lt;string&gt;         -- double-quoted
 *   | &lt;xstring&gt;        -- single-quoted
 *   | '$ &lt;name&gt;
 * </pre>
 *
 * <p> Note that this lanugage allows single-quotes as well as
 * double-quotes for strings (though the latter are called xstring in
 * the grammar).
 *
 * <p> The Language object includes three lookup tables: {@link
 * #inquirables} for lookup of relations (or primitive query names),
 * {@link #data} for translation time binding of variables, and {@link
 * #variables} for collating and re-using same named variables.
 * 
 * <p> Translation is not multi-thread safe.
 */
public class Language extends BNFGrammar {

    private static String SYNTAX =
	"rule ::= <predicate> '=> <predicate> " +

	"predicate ::= " +
	"   'And '( <predicates> ') # AND " +
	" | 'Or '( <predicates> ') # OR " +
	" | 'Lost <predicate> # LOST " +
	" | 'Not <predicate> # NOT " +
	" | 'Equals '( <arguments> ') # EQUALS " +
	" | 'Distinct '( <arguments> ') # DISTINCT " +
	" | <name> '( <arguments> ') # GET " +

	"predicates ::= <predicate> ', <predicates> # Enlist " +
	" | <predicate> # Enlist " +

	"arguments ::= <argument> ', <arguments> # Enlist " +
	" | <argument> # Enlist " +

	"argument ::= <number> # NUMBER" +
	" | <string> # STRING " +
	" | <xstring> # STRING " +
	" | '$ <name> # VARIABLE " +
	""
	;

    /**
     * Constant that describes the construction argument types for
     * value queries such as {@link Equals} and {@link Distinct}.
     */
    private final static Class [] QUERYARGVAL =
	new Class [] { Object[].class };

    /**
     * Constant that describes the construction argument types for
     * single query queries such as {@link Not} and {@link Lost}.
     */
    private final static Class [] QUERYARGONE =
	new Class [] { Query.class };

    /**
     * Constant that describes the construction argument types for
     * multi query queries such as {@link And} and {@link Or}.
     */
    private final static Class [] QUERYARGMANY =
	new Class [] { Query[].class };

    /**
     * Base class for queries taking multiple value arguments.
     */
    public class ArgsQuery extends Production {
	/**
	 * Holds the target constructor for this kind of queries.
	 */
	private Constructor cnr;

	/**
	 * Constructor for a given target class.
	 */
	public ArgsQuery(Class c) {
            try {
		cnr = c.getConstructor( QUERYARGVAL );
            } catch (Exception e) {
		e.printStackTrace();
		throw new Error( "BUG 1" );
            }
	}

	/**
	 * The rule action. Create an instance of the target class for
	 * the given arguments.
	 */
	public Object action(Vector v) {
	    try {
		v = (Vector) v.get( 0 );
		return cnr.newInstance(
		    new Object [] { v.toArray( new Object [ v.size() ] ) } );
	    } catch (Exception e) {
		e.printStackTrace();
		throw new Error( "BUG 2" );
	    }
	}
    }

    /**
     * Base class for queries taking multiple arguments.
     */
    public class ManyQuery extends Production {
	/**
	 * Holds the target constructor for this kind of queries.
	 */
	private Constructor cnr;

	/**
	 * Constructor for a given target class.
	 */
	public ManyQuery(Class c) {
            try {
		cnr = c.getConstructor( QUERYARGMANY );
            } catch (Exception e) {
		e.printStackTrace();
		throw new Error( "BUG 1" );
            }
	}

	/**
	 * The rule action. Create an instance of the target class for
	 * the given arguments.
	 */
	public Object action(Vector v) {
	    try {
		v = (Vector) v.get( 0 );
		return cnr.newInstance( new Object [] {
			(Query[]) v.toArray( new Query [ v.size() ] ) } );
	    } catch (Exception e) {
		e.printStackTrace();
		throw new Error( "BUG 2" );
	    }
	}
    }

    /**
     * Base class for queries taking a single argument.
     */
    public class OneQuery extends Production {
	/**
	 * Holds the target constructor for this kind of queries.
	 */
	private Constructor cnr;

	/**
	 * Constructor for a given target class.
	 */
	public OneQuery(Class c) {
            try {
		cnr = c.getConstructor( QUERYARGONE );
            } catch (Exception e) {
		e.printStackTrace();
		throw new Error( "BUG 1" );
            }
	}

	/**
	 * The rule action. Create an instance of the target class for
	 * the given argument.
	 */
	public Object action(Vector v) {
	    try {
		return cnr.newInstance( new Object[] { (Query) v.get( 0 ) } );
	    } catch (Exception e) {
		e.printStackTrace();
		throw new Error( "BUG 2" );
	    }
	}
    }

    /**
     * Rule action for making an {@link And} query
     */
    public class AND extends ManyQuery {
	public AND() {
	    super( And.class );
	}
    }

    /**
     * Rule action for making an {@link Or} query
     */
    public class OR extends ManyQuery {
	public OR() {
	    super( Or.class );
	}
    }

    /**
     * Rule action for making a {@link Not} query
     */
    public class NOT extends OneQuery {
	public NOT() {
	    super( Not.class );
	}
    }

    /**
     * Rule action for making a {@link Lost} query
     */
    public class LOST extends OneQuery {
	public LOST() {
	    super( Lost.class );
	}
    }

    /**
     * Rule action for making an {@link Equals} query
     */
    public class EQUALS extends ArgsQuery {
	public EQUALS() {
            super( Equals.class );
	}
    }

    /**
     * Rule action for making a {@link Distinct} query
     */
    public class DISTINCT extends ArgsQuery {
	public DISTINCT() {
            super( Distinct.class );
	}
    }

    /**
     * Rule action for making an {@link Inquirable#get} query.
     */
    public class GET extends Production {
	public Object action(Vector v) {
	    String name = (String) v.get( 0 );
	    try {
		v = (Vector) v.get( 1 );
		Inquirable inquirable = (Inquirable) inquirables.get( name );
		return inquirable.get( v.toArray( new Object[ v.size() ] ) );
	    } catch (Exception e) {
		e.printStackTrace();
		throw new Error( "Unknown Inquirable: " + name );
	    }
	}
    }

    /**
     * Rule action for making a number value.
     */
    public class NUMBER extends Production {
	/**
	 * Makes an Integer object for the given number
	 */
        public Object action(Vector v) {
	    int i = 0;
	    try {
		i = Integer.parseInt( (String) v.get( 0 ) );
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	    return Integer.valueOf( i );
        }
    }

    /**
     * Rule action for making a string value.
     */
    public class STRING extends Production {
	/**
	 * Makes a String object for the given string
	 */
        public Object action(Vector v) {
            return v.get( 0 );
        }
    }

    /**
     * Rule action for making a variable. The name is first looked up
     * in the {@link #variables} collection, and if not found, a new
     * variable is created and added to the collection. Further, if
     * the {@link #data} context is set and the variable is unbound,
     * then the variable is also initialised from the equally named
     * data element, if any.
     */
    public class VARIABLE extends Production {
	/**
	 * Makes a String object for the given string
	 */
        public Object action(Vector v) {
            String name = "$" + (String) v.get( 0 );
	    Ref x = (Ref) variables.get( name );
	    if ( x == null ) {
		x = new Ref( name );
		variables.put( name, x );
	    }
	    if ( data != null && x.get() == null ) {
		Object value = data.getValue( name );
		if ( value != null )
		    x.set( value );
	    }
	    return x;
        }
    }

    /**
     * Data context for pre-assignment of variables.
     */
    private Data data;

    /**
     * Collection of variables.
     */
    private Map variables = new HashMap();

    /**
     * Table of Inquirables.
     */
    private Map inquirables = new HashMap();

    /**
     * Constructor, which installs the language.
     */
    public Language() {
	install( SYNTAX );
	addRule( new Identifier( "name" ) );
	addRule( new QuotedString( "string", '"' ) );
	addRule( new QuotedString( "xstring", '\'' ) );
	addRule( new Number( "number" ) );
	
    }

    /**
     * Holds the singleton grammar.
     */
    private static Language grammar;

    /**
     * Returns the singleton Language object, creating it on first
     * call.
     */
    public static Language getLanguage() {
	if ( grammar == null )
	    grammar = new Language();
	return grammar;
    }

    /**
     * Utility method that translates a {@link String} into a {@link
     * Query} with given collections of inquirables and variables, and
     * data context. The inquirables are used for lookup of relation
     * symbols, and the {@link Data} context is used for initialising
     * variables. Variables mentioned in the {@link String} are
     * located from the variables collection, or created as needed.
     * The inquirables collection must define all relations that are
     * mentioned in the {@link String}, while the variables collection
     * and the data context may be given as null.
     */
    public static Query textToQuery(
	String text,Collection/*<Inquirable>*/ inquirables,
	Collection/*<Ref>*/ vars,Data settings) {
	Language g = getLanguage();
	g.inquirables = new HashMap/*<String,Inquirable>*/();
	if ( inquirables != null ) {
	    for ( Iterator/*<Inquirable>*/ si =
		      inquirables.iterator(); si.hasNext(); ) {
		Inquirable inquirable = (Inquirable) si.next();
		g.inquirables.put( inquirable.getName(), inquirable );
	    }
	}
	g.data = settings;
	g.variables = new HashMap/*<String,Ref>*/();
	if ( vars != null ) {
	    for ( Iterator/*<Ref>*/ vi = vars.iterator(); vi.hasNext(); ) {
		Ref variable = (Ref) vi.next();
		g.variables.put( variable.getName(), variable );
	    }
	}
	try {
	    return (Query) g.parseAndProcess( "predicate", text );
	} catch (Throwable t) {
	    t.printStackTrace();
	    return null;
	}
    }

    /**
     * Utility method that translates a {@link String} into a {@link
     * Rule} with a given collection of inquirables.  The inquirables
     * collection must define all relations that are mentioned in the
     * {@link String}
     */
    public static Vector/*<Query>*/ textToRule(
	String text,Collection/*<Inquirable>*/ inquirables) {
        Language g = getLanguage();
	g.inquirables = new HashMap/*<String,Inquirable>*/();
	if ( inquirables != null ) {
	    for ( Iterator/*<Inquirable>*/ si =
		      inquirables.iterator(); si.hasNext(); ) {
		Inquirable inquirable = (Inquirable) si.next();
		g.inquirables.put( inquirable.getName(), inquirable );
	    }
	}
	g.variables = new HashMap/*<String,Ref>*/();
	g.data = null;
	try {
	    return (Vector/*<Query>*/) g.parseAndProcess( "rule", text );
	} catch (Throwable t) {
	    t.printStackTrace();
	    return null;
	}
    }

}
