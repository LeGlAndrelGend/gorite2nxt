/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data.addon;

import com.intendico.data.*;
import com.intendico.gorite.addon.Reflector;
import com.intendico.gorite.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.Observer;
import java.util.Vector;

/**
 * This is a Capability extension that offers the use of predicates in
 * text form using the language defined by the {@link Language}
 * grammar.
 *
 * <p> The method {@link #textQuery(Data,String)} is used for
 * immediate {@link com.intendico.data.Query Query} construction,
 * which is suitable for the {@link Plan#context(Data)}
 * implementations. The following is an illustration of its use:
 *
 * <pre>
 * addGoal( new Plan( ... ) {
 *     public Query context(Data d) {
 *         return <b>textQuery</b>( d, "And( foo( 45, $x ), bar( $x ) )" );
 *     }
 * } );
 * </pre>
 *
 * <p> The method {@link #addRule(String)} provides a deferred {@link
 * Rule} installation from a text form rule. The following is an
 * illustration of its use:
 *
 * <pre>
 * addRule( "Lost primary($a,$b,$c) => secondary($a,$c)" );
 * </pre>
 *
 * <p> The method {@link #addReflector(String,String,String)} provides
 * a deferred {@link Reflector} installation with a text form
 * query. The following is an illustration of its use:
 *
 * <pre>
 * addReflector( "secondary lost", "percept","Lost secondary($a,$c)" );
 * </pre>
 *
 * The deferred declarations are processed and installed by the first
 * invokation of the {@link Capability#shareInquirables(Collection)}
 * method, when all the inquirables have been shared. In that way the
 * text form references get resolved to actual relations when
 * translated into the actual {@link Query} or {@link Rule} structure.
 *
 * <p> Note that more arbitrary conditions in text form predicates
 * need to be supported by appropriate {@link Inquirable}
 * implementations.  For example, the following could be a way to
 * define a computed "greater" relation between Integer values:
 *
 * <pre>
 * putInquirable( new Inquirable() {
 *     public String getName() { return "greater"; }
 *     public Query get(final Object [] args) {
 *         return new Condition() {
 *             public boolean condition() {
 *                 int a = ((Integer) Ref.deref( args[0] )).intValue();
 *                 int b = ((Integer) Ref.deref( args[1] )).intValue();
 *                 return a > b;
 *             }
 *         };
 *     }
 * } );
 * </pre>
 *
 * @see Language
 */
public class TextQueryCapability extends Capability {

    /**
     * Adds a textual rule definition. {@link Rule} creation is set up
     * to be deferred until after the first {@link
     * Capability#shareInquirables} invocation.
     */
    public void addRule(final String rule) {
	add( new Deferred() {
	    /**
	     * Installs this as a proper {@link Reflector}.
	     */
	    public void install() {
		Vector/*<Query>*/ q =
		    Language.textToRule( rule, getInquirables().values() );
		addRule( (Query) q.get( 0 ), (Query) q.get( 1 ) );
	    }
	} );
    }

    /**
     * Utility method to declare a {@link Reflector} for the owner
     * {@link Performer} using a text form query. This is deferred to
     * after the first {@link Capability#shareInquirables(Collection)}
     * as needed.
     */
    public void addReflector(
	final String goal,final String todo,final String query) {
	add( new Deferred() {
	    /**
	     * Installs this as a proper {@link Reflector}.
	     */
	    public void install() {
		Reflector r = new Reflector( getPerformer(), goal, todo );
		r.addQuery( Language.textToQuery(
				query,
				getInquirables().values(), null, null ),
			    goal );
	    }
	} );
    }

    /**
     * Utility method that creates a {@link Query} by resolving to the
     * {@link Capability#inquirables} and the given {@link Data} context.
     * Note that using this method before inquirables are shared, or more
     * specifically if relation names are undefined, may result in an
     * {@link Error}. Further, if the method returns null if the
     * predicate is not parsable.
     */
    public Query textQuery(Data data,String predicate) {
	return Language.textToQuery(
	    predicate, getInquirables().values(), null, data );
    }

}
