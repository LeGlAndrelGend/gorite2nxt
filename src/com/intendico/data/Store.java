/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Observable;

/**
 * This interface defines the requirements of a data store, which is
 * an object that contains "tuples" represented as Object arrays. Note
 * that stores typically should implement {@link Inquirable} as well.
 */
public interface Store {

    /**
     * Interface method for adding a tuple of values to this
     * Store. The method returns true if the store was modified due to
     * this action, otherwise false.
     */
    public boolean add(Object/*...*/ [] values) throws Exception ;

    /**
     * Interface method for removing a tuple of values from this
     * Store. The method returns true if the store was modified due to
     * this action, otherwise false.
     */
    public boolean remove(Object/*...*/ [] values) throws Exception ;

    /**
     * Interface method for clearing this store of all its data.
     */
    public void clear() throws Exception ;

    /**
     * Interface method for returning an Observable that notifies
     * about subsequent changes to the Store.
     */
    public Observable getMonitor();
}
