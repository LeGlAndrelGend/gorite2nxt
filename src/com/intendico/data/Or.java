/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Observer;
import java.util.Vector;

/**
 * The Or class implements a disjunction of multiple queries.
 */
public class Or implements Query {

    /**
     * Holds the disjuncts being combined.
     */
    public Query [] disjuncts;

    /**
     * Keeps track of which query is current.
     */
    public int current;

    /**
     * Constructor.
     */
    public Or(Query/*...*/ [] q)
	throws Exception {
	disjuncts = q;
	reset();
    }

    /**
     * The {@link Query#copy} method implemented by creating a new
     * Or object with copies of the disjuncts.
     */
    public Query copy(Vector/*<Ref>*/ newrefs) throws Exception {
	Query [] q = new Query [ disjuncts.length ];
	for ( int i = 0; i < q.length; i++ ) {
	    q[i] = disjuncts[i].copy( newrefs );
	}
	return new Or( q );
    }

    /**
     * The {@link Query#reset} method implemented by reverting to the
     * first disjunct and resetting it.
     */
    public void reset()
	throws Exception
    {
	current = 0;
	disjuncts[ 0 ].reset();
    }

    /**
     * The {@link Query#next} method implemented by considering all
     * disjucts one by one to the next match in a left-to-right fashion.
     */
    public boolean next()
	throws Exception
    {
	while ( current < disjuncts.length ) {
	    if ( disjuncts[ current ].next() )
		return true;
	    current += 1;
	    if ( current >= disjuncts.length )
		break;
	    disjuncts[ current ].reset();
	}
	return false;
    }

    /**
     * The {@link Query#getRefs} method implemented by combining the
     * Ref objects of all disjuncts.
     */
    public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v) {
	for ( int i=0; i < disjuncts.length; i++ )
	    disjuncts[ i ].getRefs( v );
	return v;
    }

    /**
     * The {@link Query#addObserver} method implemented by adding
     * the observer to all the disjuncts.
     */
    public void addObserver(Observer x) {
	for ( int i=0; i < disjuncts.length; i++ )
	    disjuncts[ i ].addObserver( x );
    }

    /**
     * The {@link Query#deleteObserver} method implemented by
     * removing the observer from all the disjuncts.
     */
    public void deleteObserver(Observer x) {
	for ( int i=0; i < disjuncts.length; i++ )
	    disjuncts[ i ].deleteObserver( x );
    }

    /**
     * The {@link Query#addable} method implemented by forwarding to all
     * disjuncts.
     */
    public boolean addable()
    {
	for ( int i=0; i < disjuncts.length; i++ )
            if ( disjuncts[ i ].addable() )
		return true;
	return false;
    }

    /**
     * The {@link Query#add} method implemented by forwarding to first
     * addable disjunct.
     */
    public boolean add()
    {
	for ( int i=0; i < disjuncts.length; i++ ) {
	    if ( disjuncts[ i ].addable() )
		return disjuncts[ i ].add();
	}
	return false;
    }

    /**
     * Implements {@link Query#removable} by checking that all
     * disjuncts are removable.
     */
    public boolean removable()
    {
	for ( int i=0; i < disjuncts.length; i++ )
	    if ( ! disjuncts[ i ].removable() )
		return false;
	return true;
    }

    /**
     * Implements {@link Query#remove} by forwarding to all disjuncts.
     */
    public boolean remove() {
	boolean removed = false;
	for ( int i=0; i < disjuncts.length; i++ )
	    if ( disjuncts[ i ].removable() )
		removed |= disjuncts[ i ].remove();
	return removed;
    }

    /**
     * Returns a {@link String} representation of the Or object.
     */
    public String toString() {
	StringBuffer s = new StringBuffer();
	s.append( "Or(" );
	for ( int i=0; i < disjuncts.length; i++ ) {
	    if ( i > 0 )
		s.append( "," );
	    s.append( " " );
	    s.append( disjuncts[ i ].toString() );
	}
	s.append( " )" );
	return s.toString();
    }
}
