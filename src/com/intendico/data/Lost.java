/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Observer;
import java.util.Vector;

/**
 * The Lost class is a {@link Snapshot} that enumerates the lost
 * tuples rather than the added tuples, and it also treats adding and
 * removing like {@link Not}.
 */
public class Lost extends Snapshot {

    /**
     * Constructor;
     */
    public Lost(Query q) {
	super( q );
    }

    /**
     * Alternative constructor with synchronization object.
     */
    public Lost(Object s,Query q) {
	super( s, q );
    }

    /**
     * The {@link Query#copy} method implemented by creating a new
     * Lost with a copy of the monitored {@link #query}, and sharing
     * any {@link #sync} object.
     */
    public Query copy(Vector/*<Ref>*/ newrefs) throws Exception {
	return new Lost( sync, query.copy( newrefs ) );
    }

    /**
     * The {@link Query#reset} method implemented by forwarding to the
     * wrapped query.
     */
    public void reset() throws Exception {
	super.reset();
	bindings = lost.iterator();
    }

    /**
     * Implements {@link Query#addable} by calling {@link
     * Query#removable} on the wrapped query.
     */
    public boolean addable() {
	return query.removable();
    }

    /**
     * Implements {@link Query#add} by calling {@link Query#remove} on
     * the wrapped query.
     */
    public boolean add() {
	return query.remove();
    }

    /**
     * Implements {@link Query#removable} by calling {@link
     * Query#addable} on the wrapped query.
     */
    public boolean removable() {
	return query.addable();
    }

    /**
     * Implements {@link Query#remove} by calling {@link Query#add} on
     * the wrapped query.
     */
    public boolean remove() {
	return query.add();
    }

    /**
     * Returns the textual representation of the wrapped query, within
     * parentheses.
     */
    public String toString() {
	return "Lost(" + query + ")";
    }
}
