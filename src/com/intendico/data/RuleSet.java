/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Observable;
import java.util.Observer;
import java.util.Vector;
import java.util.HashSet;
import java.util.Iterator;

/**
 * A RuleSet is a reason maintenance unit, which attempts to uphold
 * given rules. In normal use, the RuleSet is populated with some
 * number of Rule objects, via either {@link #add(Rule)} or {@link
 * #add(Query,Query)}, which tie the {@link Rule} objects to the
 * RuleSet. Each {@link Rule} object, qua {@link Observer}, gets
 * notified about possible changes by their antecedent source , which
 * result in them invoking the {@link #activate} method. The RuleSet
 * {@link #run} method then pops and processes all activated rules to
 * achieve their belief propagation effects.
 */
public class RuleSet extends Observable implements Runnable {

    /**
     * Utility class to observe source update
     */
    class RuleObserver implements Observer {

	/**
	 * The {@link Rule} concerned.
	 */
	Rule rule;

	/**
	 * Constructor.
	 */
	RuleObserver(Rule r) {
	    rule = r ;
	}

	/**
	 * Implements {@link Observer#update} by activating the rule
	 * concerned. However, if the {@link Rule} is not contained in
	 * {@link #all_rules} then this observer just deletes itself
	 * from the calling {@link Observable}.
	 */
	public void update(Observable x,Object y) {
	    if ( ! all_rules.contains( rule ) ) {
		x.deleteObserver( this );
	    } else {
		activate( rule );
	    }
	}

	/**
	 * Returns a String representation of this object.
	 */
	public String toString() {
	    return "RuleObserver: " + rule;
	}
    }

    /**
     * The collection of {@link Rule} objects explicitly added to this
     * RuleSet.
     */
    public HashSet/*<Rule>*/ all_rules = new HashSet/*<Rule>*/();

    /**
     * The current set of activated rules.
     */
    public HashSet/*<Rule>*/ active = new HashSet/*<Rule>*/();

    /**
     * Utility method to activate a {@link Rule} in this RuleSet. This
     * includes adding the {@link Rule} to the {@link #active}
     * collection, then both {@link Object#notify} on that object, and
     * invoke {@link Observable#notifyObservers} to signal that this
     * RuleSet has activated rules.
     * @see #run
     */
    public void activate(Rule r) {
	synchronized ( active ) {
	    if ( ! active.contains( r ) ) {
		active.add( r );
		active.notifyAll();
		setChanged();
		notifyObservers();
	    }
	}
    }

    /**
     * Returns true if there are any activated rules.
     */
    public boolean hasActive() {
	return active.size() > 0;
    }

    /**
     * This is a flag to signal that the run method should keep
     * waiting for next update rather than returning.
     */
    public boolean wait = false;

    /**
     * The reset method calls {@link Rule#reset} on all given rules so
     * as to reset their antecedent snapshots.
     */
    public void reset(Vector/*<Rule>*/ rules) throws Exception {
	for ( Iterator/*<Rule>*/ i = rules.iterator(); i.hasNext(); ) {
	    Rule r = (Rule) i.next();
	    r.reset();
	}
    }

    /**
     * The propagate method calls {@link Rule#propagate} on all rules
     * so as to apply the rules for all their new bindings, which
     * previosuly have been snapshot by {@link Rule#reset} calls.
     */
    public void propagate(Vector/*<Rule>*/ rules) throws Exception {
        for ( Iterator/*<Rule>*/ i = rules.iterator(); i.hasNext(); ) {
            Rule r = (Rule) i.next();
	    r.propagate();
	}
    }

    /**
     * Returns a String representation of this RuleSet.
     */
    public String toString() {
	StringBuilder s = new StringBuilder();
	s.append( "{" );
	boolean first = true;
        for ( Iterator/*<Rule>*/ i = all_rules.iterator(); i.hasNext(); ) {
            Rule r = (Rule) i.next();
	    if ( ! first )
		s.append( "; " );
	    else
		first = false;
	    s.append( r.toString() );
	}
	s.append( "}" );
	return s.toString();
    }

    /**
     * Utility method to add a {@link Rule} object and tie it to this
     * RuleSet.
     */
    public void add(Rule r) {
	if ( all_rules.contains( r ) ) {
	    return;
	}
	all_rules.add( r );
	r.antecedent.addObserver( new RuleObserver( r ) );
    }

    /**
     * Utility method to create a new {@link Rule} object with given
     * antecedent and consequent {@link Query} objects, and add it to
     * this RuleSet.
     */
    public void add(Query a,Query c) {
	add( new Rule( a, c ) );
    }

    /**
     * Deactivates this rule.
     */
    public void cancel(Rule r) {
	all_rules.remove( r );
    }

    /**
     * Utility method that adds all currently active rules to a
     * {@link Vector}, and clears the active set.
     */
    public boolean popActivated(Vector/*<Rule>*/ v,boolean w) {
	synchronized ( active ) {
	    while ( active.size() == 0 ) {
		if ( w ) {
		    try {
			active.wait();
		    } catch (InterruptedException e) {
		    }
		} else {
		    return false;
		}
	    }
	    v.addAll( active );
	    active.clear();
	}
	return true;
    }

    ///
    /// Runnable implementation
    ///

    /**
     * Utility method to keep refreshing the rules if and while there
     * are source updates. The {@link #wait} flag controls whether
     * this method should block and wait for a new update (true) or
     * return (false) when a propagation session ends. Note that if
     * there are rules to propagate competing assertions in a cyclic
     * manner, then the propagation will not end and the overall
     * application will suffer badly.
     */
    public void run() {
	try {
	    Vector/*<Rule>*/ rules = new Vector/*<Rule>*/();
	    for ( ;; ) {
		if ( ! popActivated( rules, wait ) )
		    return;
		reset( rules );
		propagate( rules );
		rules.clear();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

}
