/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Vector;
import java.util.Iterator;

/**
 * This class is a carrier of values. The Relation class knows of Ref
 * objects, so as to allow passing of values between queries.
 */
public class Ref/*<T>*/ {

    /**
     * Utility method to obtain an actual value of a possible Ref
     * object.
     */
    public static Object deref(Object x) {
	return x instanceof Ref? ((Ref) x).get() : x;
    }

    /**
     * Utility method that creates an array of Object values from an
     * array of Ref
     */
    public static Object [] getValues(Ref [] rs) {
	Object [] v = new Object [ rs.length ];
	for ( int i = 0; i < rs.length; i++ ) {
	    v[ i ] = rs[ i ].get();
	}
	return v;
    }

    /**
     * Utility method to set an array of Ref from an array of Object
     * values.
     */
    //@SuppressWarnings("unchecked")
    public static void setValues(Ref [] rs,Object [] v) {
	for ( int i = 0; i < rs.length; i++ ) {
	    rs[ i ].set( Ref.deref( v[ i ] ) );
	}
    }

    /**
     * Utility method that collects the bindings of given {@link Ref}
     * objects into a {@link java.util.Vector}.
     */
    public static Vector getBinding(Vector/*<Ref>*/ refs) {
	Vector/*<Object>*/ v = new Vector/*<Object>*/();
	for ( Iterator/*<Ref>*/ i = refs.iterator(); i.hasNext(); )
	    v.add( ((Ref) i.next()).get() );
	return v;
    }

    /**
     * Utility method that binds the given {@link Ref} objects with
     * corresponding values from a {@link java.util.Vector}.
     */
    //@SuppressWarnings("unchecked")
    public static void bind(Vector/*<Ref>*/ refs,Vector v) {
	int c = 0;
	for ( Iterator/*<Ref>*/ i = refs.iterator(); i.hasNext(); )
	    ((Ref) i.next()).set( deref( v.get( c++ ) ) );
    }

    /**
     * Utility method to create a vector of ref objects from a string
     * array of names.
     */
    public static Vector/*<Ref>*/ create(String [] names) {
	Vector/*<Ref>*/ v = new Vector/*<Ref>*/();
	if ( names == null )
	    return v;
	for ( int i = 0; i < names.length; i++ ) {
	    v.add( new Ref( names[i] ) );
	}
	return v;
    }

    /**
     * Utility method that invokes {@link #clear} for the given {@link
     * Ref} objects with.
     */
    public static void clear(Vector/*<Ref>*/ refs) {
	for ( Iterator/*<Ref>*/ i = refs.iterator(); i.hasNext(); )
	    ((Ref) i.next()).clear();
    }

    /**
     * Creates copies of Ref objects.
     */
    //@SuppressWarnings("unchecked")
    public static Vector/*<Ref>*/ copy(Vector/*<Ref>*/ src) {
	Vector/*<Ref>*/ refs = new Vector/*<Ref>*/();
	for ( Iterator/*<Ref>*/ i = src.iterator(); i.hasNext(); )
	    refs.add( new Ref( (Ref) i.next() ) );
	return refs;
    }

    /**
     * Returns a textual representation of a vector of {@link Ref}
     * objects.
     */
    public static String toString(Vector/*<Ref>*/ refs) {
	return toString( (Ref[]) refs.toArray( new Ref [ refs.size() ] ) );
    }

    /**
     * Returns a textual representation of an expected array of Ref
     * objects. Each Ref object is presented by name and value, using
     * {@link #toString()}, but the value is clipped to at most 40
     * characters. The whole things is put within a matching pair of
     * curly braces.
     */
    public static String toString(Object [] refs) {
	StringBuffer s = new StringBuffer();
	s.append( "{" );
	for ( int i = 0; i < refs.length; i++ ) {
	    if ( i > 0 )
		s.append( "," );
	    if ( refs[ i ] instanceof Ref ) {
		Ref r = (Ref) refs[ i ];
		s.append( r.getName() );
		s.append( "=" );
		String v = r.toString();
		if ( v.length() > 40 )
		    v = v.substring( 0, 37 ) + "...";
		s.append( v );
	    } else {
		s.append( refs[ i ] );
	    }
	}
	s.append( "}" );
	return s.toString();
    }

    /**
     * Two Ref vectors are equals if they are of same size, and
     * dereference to the same values in the same order.
     */
    public static boolean equals(Vector/*<Ref>*/ a,Vector/*<Ref>*/ b) {
	if ( a.size() != b.size() )
	    return false;
	for ( int i = 0; i < a.size(); i++ ) {
	    Object ra = deref( a.get( i ) );
	    Object rb = deref( b.get( i ) );
	    if ( ra == null || rb == null )
		if ( ra != rb )
		    return false;
	    if ( ! ra.equals( rb ) )
		return false;
	}
	return true;
    }

    /**
     * The name given to the {@link Ref} object at construction.
     */
    public String name;

    /**
     * The value of the {@link Ref} object, or <em>null</em> if not
     * bound.
     */
    private /*T*/ Object value;

    /**
     * Constructor. Each Ref object is named.
     */
    public Ref(String n) {
	name = n;
    }

    /**
     * Constructor with both name and value.
     */
    public Ref(String n,/*T*/ Object v) {
	name = n;
	value = v;
    }

    /**
     * Constructor that copies another Ref object.
     */
    public Ref(Ref/*<T>*/ src) {
	name = src.getName();
	value = src.get();
    }

    /**
     * Finds a same named Ref object in a {@link Vector}.
     */
    public Ref find(Vector/*<Ref>*/ refs) {
	for ( Iterator/*<Ref>*/ i = refs.iterator(); i.hasNext(); ) {
	    Ref ref = (Ref) i.next();
	    if ( name.equals( ref.name ) )
		return ref;
	}
	return null;
    }

    /**
     * Returns the value. This is <em>null</em> unless bound to
     * something.
     */
    public /*T*/ Object get() {
	return value;
    }

    /**
     * Accessor method for the name.
     */
    public String getName() {
	return name == null? "Ref#" + System.identityHashCode( this ) : name;
    }

    /**
     * Assigns the value.
     */
    public void set(/*T*/ Object v) {
	value = v;
    }

    /**
     * Resets its value to null.
     */
    public void clear() {
	value = null;
    }

    /**
     * Returns a String representation of this {@link Ref} object. The
     * object is transparent, and it presents itself only via its
     * value. Use {@link #getName} to present the {@link Ref} object.
     */
    public String toString() {
	return "" + value;
    }

}
