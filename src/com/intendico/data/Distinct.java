/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Vector;
import java.util.HashSet;

/**
 * The Distinct class is a query to ensure that a collection of given
 * {@link Ref} objects have distinct bindings.
 */
public class Distinct extends Condition {

    /**
     * The Ref objects concerned.
     */
    public Object [] refs;

    /**
     * Constructor;
     */
    public Distinct(Object/*...*/ [] r) {
	refs = r;
    }

    /**
     * The {@link Query#copy} method implemented by creating a new
     * Distinct object with the copies of the refs.
     */
    //@SuppressWarnings("unchecked")
    public Query copy(Vector/*<Ref>*/ newrefs) throws Exception {
	Object [] nrefs = new Object [ refs.length ];
	for ( int i = 0; i < refs.length; i++ ) {
	    if ( refs[ i ] instanceof Ref ) {
		nrefs[ i ] = ((Ref) refs[ i ]).find( newrefs );
	    } else {
		nrefs[ i ] = refs[ i ];
	    }
	}
	return new Distinct( nrefs );
    }

    /**
     * The distinction condition
     * @return <em>false</em> if any {@link Ref} object is
     * <em>null</em>, or equal to any other {@link Ref} object,
     * otherwise <em>true</em>.
     */
    public boolean condition() {
	HashSet/*<Object>*/ v = new HashSet/*<Object>*/();
	for ( int i = 0; i < refs.length; i++ ) {
	    Object r = refs[ i ];
	    Object x = Ref.deref( r );
	    if ( x == null || v.contains( x ) )
		return false;
	    v.add( x );
	}
	return true;
    }

    /**
     * The {@link Query#getRefs} method implemented by adding any Ref
     * object not already in the vector.
     */
    public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v) {
	for ( int i = 0; i < refs.length; i++ ) {
	    Object r = refs[ i ];
	    if ( r instanceof Ref ) {
		if ( ! v.contains( (Ref) r ) )
		    v.add( (Ref) r );
	    }
	}
	return v;
    }

    /**
     * Returns the textual representation of the query.
     */
    public String toString() {
	return "Distinct(" + Ref.toString( refs ) + ")";
    }
}
