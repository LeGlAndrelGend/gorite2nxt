/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Vector;

/**
 * The Equals class is a query to ensure that two or more given
 * objects are equal, where any or both of the objects may be {@link
 * Ref} objects.  If a {@link Ref} object is null, it is set to the
 * other value.
 */
public class Equals extends Condition {

    /**
     * The distinct values concerned.
     */
    public Object [] values;

    /**
     * Constructor.
     */
    public Equals(Object/*...*/ [] v) {
	values = v;
    }

    /**
     * The {@link Query#copy} method implemented by creating a new
     * Except object with a copy of the ref, but the same values
     */
    //@SuppressWarnings("unchecked")
    public Query copy(Vector/*<Ref>*/ newrefs) throws Exception {
	Object [] v = new Object [ values.length ];
	for ( int i = 0; i < values.length; i++ ) {
	    if ( values[ i ] instanceof Ref ) {
		v[ i ] = ((Ref)values[ i ]).find( newrefs );
	    } else {
		v[ i ] = values[ i ];
	    }
	}
	return new Equals( v );
    }

    /**
     * The exception condition. 
     * @return <em>false</em> if the {@link Ref} object is equal to
     * any value, otherwise <em>true</em>. Note that any Ref object as
     * value is also dereferenced.
     */
    public boolean condition() {
	Object x = null;
	for ( int i = 0; i < values.length; i++ ) {
	    Object value = values[ i ];
	    Object y =  Ref.deref( value );
	    if ( x != null && ! x.equals( y ) )
		return false;
	    x = y;
	}
	return true;
    }

    /**
     * The {@link Query#getRefs} method implemented by adding and Ref
     * not already contained in the vector.
     */
    public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v) {
	for ( int i = 0; i < values.length; i++ ) {
            Object x = values[ i ];
	    if ( x instanceof Ref && ! v.contains( x ) )
		v.add( (Ref) x );
	}
	return v;
    }

    /**
     * Returns the textual representation of the query.
     */
    public String toString() {
	StringBuilder s = new StringBuilder( "equals(" );
	for ( int i = 0; i < values.length; i++ ) {
            Object x = values[ i ];
	    s.append( "," );
	    if ( x instanceof Ref ) {
		s.append( ((Ref) x).getName() );
		s.append( "=" );
	    }
	    s.append( x.toString() );
	}
	s.append( ")" );
	return s.toString();
    }
}
