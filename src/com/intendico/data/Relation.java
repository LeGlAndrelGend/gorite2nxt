/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

/**
 * A Relation is a set of tuples of the same size. It is created by
 * enumerating the types of its columns.
 *
 * <p> A relation may have one or more <em>key constraints</em>. A key
 * constraint combines one or more fields into a <em>key</em>, which
 * the relation may hold only one tuple for any combination of
 * values. The key constraints are used by the {@link #add} method,
 * such that addition of a new tuple may cause the removal of prior
 * tuples that are equal in some key combination of fields.
 *
 * <p> A relation is modified via the {@link #add} and {@link #remove}
 * methods. Both of these notify observers if the relation is changed.
 *
 * <p> A relation can be queried via the {@link #query} method, which
 * returns an {@link java.util.Iterator} for the matching tuples, or
 * by the {@link #get} method, which instead returns a {@link
 * Relation.Cursor} that implements the {@link Query} interface.
 */
public class Relation extends Observable implements Store, Inquirable {

    /**
     * Utility method to create a copy of an object array. Only the
     * array is copied, not its values.
     */
    public static Object [] copyArray(Object[] in)
    {
	Object [] out = new Object [ in.length ];
	System.arraycopy( in, 0, out, 0, in.length);
	return out;
    }

    /**
     * An instance name for this relation.
     */
    public final String name;

    /**
     * The required field types.
     */
    public final Class [] column_types;

    /**
     * The special key pattern for all fields being key.
     */
    public final KeyPattern main_key;

    /**
     * Holds the relation data in a layered structure. First hash
     * index is the key pattern, and second hash index is the key (of
     * a pattern). Actual data is a HashSet of Tuple objects, which
     * are compared by using {@link java.lang.Object#equals} of their
     * fields.
     */
    public Hashtable/*<KeyPattern,Hashtable<Tuple,HashSet<Tuple>>>*/ indexes =
	new Hashtable/*<KeyPattern,Hashtable<Tuple,HashSet<Tuple>>>*/();

    /**
     * Holds the collection of constraining key patterns. When a tuple
     * is added, any already existing tuple of the same key (according
     * to any constraining key pattern) is seen as an opposing tuple,
     * which is removed (without observer notification).
     */
    public HashSet/*<KeyPattern>*/ constraints;

    /**
     * The ArityException is thrown when a relation is accessed with a
     * wrong number of fields.
     */
    public class ArityException extends Exception {

	/**
	 * Version identity required for serialization.
	 */
	public static final long serialVersionUID = 1L;

	/**
	 * Telling the required number of fields.
	 */
	public final int requires;

	/**
	 * Telling the given number of fields.
	 */
	public final int attempted;

	/**
	 * Constructor.
	 */
	public ArityException(int r,int a) {
	    requires = r;
	    attempted = a;
	}
    }

    /**
     * The ColumnTypeException exception is thrown when a realtion is
     * accessed by means of wrong field types.
     */
    public class ColumnTypeException extends Exception {

	/**
	 * Version identity required for serialization.
	 */
	public static final long serialVersionUID = 1L;

	/**
	 * The required type of the first erroneous field.
	 */
	public final Class requires;

	/**
	 * The given type of the first erroneous field.
	 */
	public final Class attempted;

	/**
	 * The field index.
	 */
	public final int index;

	/**
	 * Constructor.
	 */
	public ColumnTypeException(int i,Class r,Class a) {
            requires = r;
            attempted = a;
	    index = i;
	}
    }

    /**
     * Constructor. This defines both the relation arity and the
     * individual types of the fields.
     */
    public Relation(String n,Class/*...*/ [] types) {
	name = n;
	column_types = types;
	main_key = new KeyPattern();
    }

    /**
     * Alternative constructor with name and arity only.
     */
    public Relation(String n,int arity) {
	name = n;
	column_types = new Class [ arity ];
	for ( int i = 0; i < arity; i++ ) {
	    column_types[ i ] = Object.class;
	}
	main_key = new KeyPattern();
    }

    /**
     * Clears the relation, i.e. removes all its tuples. Its key
     * constraints are unchanged.
     */
    public void clear() {
	indexes =
	    new Hashtable/*<KeyPattern,Hashtable<Tuple,HashSet<Tuple>>>*/();
	setChanged();
	notifyObservers();
    }

    /**
     * Returning an Observable that notifies about subsequent changes
     * to this Relation.
     */
    public Observable getMonitor() {
	return this;
    }

    /**
     * Adds a constraining key pattern. The relation is managed to
     * allow at most one tuple of any key of the constraining key
     * patterns. This is enforced when new tuples are added, which
     * thus results in that a prior tuple of the same key is removed
     * (without observer notification).
     *
     * <p> Note that a relation that violates the key constraint when
     * the constraint is added will not be corrected until an opposing
     * tuple is added. It does make good sense to add all key
     * constraints before any tuples are added.
     */
    public Relation addConstraint(boolean/*...*/ [] pattern)
	throws ArityException
    {
	if ( constraints == null )
	    constraints = new HashSet/*<KeyPattern>*/();
	constraints.add( new KeyPattern( pattern ) );
	return this;
    }

    /**
     * Removes a constraining key pattern.
     * 
     * <p> Note that the general advice is <em>aginst</em> removing
     * key constraints, but rather creating a new relation with the
     * reduced set of constraints and then populate that.
     */
    public void removeConstraint(boolean/*...*/ [] pattern)
	throws ArityException
    {
	if ( constraints == null )
	    return;
	constraints.remove( new KeyPattern( pattern ) );
	if ( constraints.size() == 0 )
	    constraints = null;
    }

    /**
     * Utility method to remove all tuples opposed to a given tuple
     * according to a given key pattern. Tuples are removed without
     * observer notification.
     */
    public void applyConstraint(KeyPattern constraint,Tuple adding)
	throws ArityException, ColumnTypeException
    {
	Tuple key = constraint.getKey( adding );
	for ( Iterator/*<Tuple>*/ i = query( key ); i.hasNext(); ) {
	    addPending( (Tuple) i.next() );
	}
	processPending( false );
    }

    /**
     * Utility method to remove all tuples that are opposite to the
     * given tuple with respect to the constraining key patterns.
     * Tuples are removed without observer notification.
     */
    public void applyConstraints(Tuple adding)
	throws ArityException, ColumnTypeException
    {
	if ( constraints == null )
	    return;
	for ( Iterator/*<KeyPattern>*/ i =
		  constraints.iterator(); i.hasNext(); ) {
	    applyConstraint( (KeyPattern) i.next(), adding );
	}
    }

    /**
     * Utility method that investigates that fields are appropriate
     * for the relation. Any violations are reported by throwing the
     * appropriate exception.
     * @throws ArityException if there is a wrong number of fields.
     * @throws ColumnTypeException if any field is of wrong type.
     */
    public void runtimeTyping(Object/*...*/ [] fields)
	throws ArityException, ColumnTypeException
    {
	if ( column_types == null ) {
	    if ( fields == null )
		return;
	    throw new ArityException( 0, fields.length );
	}

	if ( fields == null )
	    throw new ArityException( column_types.length, 0 );

	if ( fields.length != column_types.length )
	    throw new ArityException( column_types.length, fields.length );

	for ( int i = 0; i < fields.length; i++ ) {
	    Object x = Ref.deref( fields[ i ] );
	    if ( x == null )
		continue;
	    if ( column_types[ i ].isInstance( x ) )
		continue;
	    throw new ColumnTypeException(
		i, column_types[ i ], x.getClass() );
	}
    }

    
    /**
     * Represents an I/O key pattern for the relation. This is used
     * for identifying keyed access tables.
     */
    public class KeyPattern {

	/**
	 * The I/O pattern expressed as a boolean array. A true means
	 * input, and a false means output.
	 */
	boolean [] pattern;

	/**
	 * Constructor by boolean indicators. True indicates an input
	 * field, and false indicates an output field.
	 */
	public KeyPattern(boolean/*...*/ [] io)
	    throws ArityException
	{
	    if ( io.length != column_types.length )
		throw new ArityException( column_types.length, io.length );
	    pattern = io;
	}

	/**
	 * Default constructor, with all fields key fields.
	 */
	public KeyPattern() {
	    pattern = new boolean[ column_types.length ];
	    for ( int i = 0; i < pattern.length; i++ )
		pattern[ i ] = false;
	}

	/**
	 * Constructor by field presence. The constructor is called
	 * with the right number of fields, leaving output fields as
	 * null and input fields non-null.
	 */
	public KeyPattern(Object/*...*/ [] io)
	    throws ArityException
	{
	    if ( io.length != column_types.length )
		throw new ArityException( column_types.length, io.length );
	    // check types as well?
	    pattern = new boolean [ io.length ];
	    for ( int i = 0; i < io.length; i++ )
		pattern[ i ] = Ref.deref( io[ i ] ) != null;
	}

	/**
	 * Equality of KeyPattern objects is determined by comparing
	 * their boolean patterns.
	 */
	public boolean equals(Object p) {
	    return p instanceof KeyPattern ? equals( (KeyPattern) p ) : false ;
	}

	/**
	 * Equality of KeyPattern objects is determined by comparing
	 * their boolean patterns.
	 */
	public boolean equals(KeyPattern p) {
	    if ( pattern == null || p.pattern == null )
		return pattern == null && p.pattern == null;
	    if ( pattern.length != p.pattern.length )
		return false;
	    for ( int i = 0; i < pattern.length; i++ )
		if ( pattern[ i ] != p.pattern[ i ] )
		    return false;
	    return true;
	}

	/**
	 * The hashCode of a KeyPattern is derived by treating its
	 * boolean pattern as a bit pattern.
	 */
	public int hashCode() {
	    int code = 0;
	    for ( int i = 0; i < pattern.length; i++ )
		code = ( code << 1 ) + ( pattern[ i ]? 1 : 0 );
	    return code;
	}

	/**
	 * Returns a tuple by projecting the given tuple through this
	 * key pattern.
	 */
	public Tuple getKey(Tuple t) {
	    return new Tuple( t, pattern );
	}

	/**
	 * Returns a String representation of the KeyPattern object.
	 */
	public String toString() {
	    StringBuffer s = new StringBuffer();
	    String sep = "";
            for ( int i = 0; i < pattern.length; i++ ) {
		s.append( sep );
		s.append( pattern[ i ] );
		sep = ",";
	    }
	    return "KeyPattern(" + s.toString() +")";
	}
    }

    /**
     * Representation of relation row. In a contents row, all fields
     * of a tuple should be non-<em>null</em>. Tuples that represent
     * access patterns typically have <em>null</em>-valued {@link Ref}
     * objects, or are <em>null</em> directly, for the output fields,
     * whereas the input fields provide values to match with.
     */
    public class Tuple {
	
	/**
	 * Holds the field values.
	 */
	public Object [] fields;

	/**
	 * Constructor.
	 */
	public Tuple() {
	    fields = new Object[ column_types.length ];
	}

	/**
	 * Constructor.
	 */
	public Tuple(Object/*...*/ [] tuple) {
	    fields = copyArray( tuple );
	}

	/**
	 * Returns the number of <em>null</em> fields of this tuple.
	 */
	public int nullCount() {
	    int x = 0;
	    for (int i = 0; i < fields.length; i++) {
		if ( Ref.deref( fields[ i ] ) == null )
		    x += 1;
	    }
	    return x;
	}

	/**
	 * Key constructor. Creates a copy of the given tuple, but
	 * clears the output fields, as given by the key pattern.
	 */
	public Tuple(Tuple t,boolean [] pattern) {
	    this();
	    for (int i = 0; i < fields.length; i++) {
		if ( pattern[ i ] )
		    fields[ i ] = Ref.deref( t.fields[ i ] );
		else if ( fields[ i ] instanceof Ref )
		    ((Ref) fields[ i ]).clear();
	    }
	}

	/**
	 * Utility method to clear this tuple according to the key
	 * pattern.
	 */
	public void clear(boolean [] pattern) {
	    for ( int i = 0; i < pattern.length; i++ ) {
		if ( pattern[ i ] )
		    continue;
		if ( fields[ i ] instanceof Ref )
		    ((Ref) fields[ i ] ).clear();
		else 
		    fields[ i ] = null;
	    }
	}

	/**
	 * Determine whether a given query matches this tuple, by
	 * comparing all non-null query fields with the tuple fields
	 * using the equals method.
	 *
	 * @return <em>true</em> if this tuple matches the given query
	 * tuple when ignoring its <em>null</em> fields, and
	 * <em>false</em> otherwise.
	 */
	public boolean match(Tuple query) {
	    for ( int i = 0; i < fields.length; i++ ) {
		Object x = Ref.deref( query.fields[ i ] );
		if ( x == null )
		    continue;
		if ( ! x.equals( Ref.deref( fields[ i ] ) ) )
		    return false;
	    }
	    return true;
	}

	/**
	 * Fill in the fields of a query from this tuple using the
	 * given key pattern.
	 */
	//@SuppressWarnings("unchecked")
	public void getFields(KeyPattern kp, Object [] output) {
	    for ( int i = 0; i < fields.length; i++ ) {
		if ( ! kp.pattern[ i ] ) {
		    if ( output[ i ] instanceof Ref )
			((Ref) output[ i ]).set( fields[ i ] );
		    else
			output[ i ] = fields[ i ];
		}
	    }
	}

	/**
	 * Returns a given field value.
	 */
	public Object getField(int index) {
	    return fields[ index ];
	}

	/**
	 * Determine whether this tuple is equal to a given object.
	 */
	public boolean equals(Object x) {
	    return x instanceof Tuple? equals( (Tuple) x ) : false;
	}

	/**
	 * Determine whether this tuple is equal to another. The
	 * method dereferences any {@link Ref} objects of both this
	 * tuple and the other tuple.
	 *
	 * @return <em>true</em> if all field values (after
	 * dereference) are equal, and <em>false</em> otherwise.
	 */
	public boolean equals(Tuple x) {
	    for ( int i = 0; i < fields.length; i++ ) {
		if ( ! equalFields(
			 Ref.deref( fields[ i ] ),
			 Ref.deref( x.fields[ i ] ) ) )
		    return false;
	    }
	    return true;
	}

	/**
	 * Utility method to compare field values that may be
	 * <em>null</em>.
	 *
	 * @return <em>true</em> if the field values are equal, and
	 * <em>false</em> otherwise.
	 */
	public boolean equalFields(Object a,Object b) {
	    return ( a == null || b == null )? a == b : a.equals( b );
	}

	/**
	 * Compute a tuple hash code, by combining the dereferenced
	 * field values in a deterministic way.
	 *
	 * <p> Note that tuples may have the same hash code without
	 * being equal.
	 *
	 * @return the hash code.
	 */
	public int hashCode() {
	    int x = 0;
	    for ( int i = 0; i < fields.length; i++ ) {
		x <<= 2 ;
		x += ( Ref.deref( fields[ i ] ) == null?
		       0 :
		       Ref.deref( fields[ i ] ).hashCode() );
	    }
	    return x;
	}

	/**
	 * Support method to queue up this tuple for removal from its
	 * relation. Use method {@link #processPending} to process
	 * delayed removals.
	 *
	 * <p> This method is intended to simplify the combination of
	 * iterating over several tuples of the relation while
	 * removing some of them, without this causing problems due to
	 * {@link java.util.ConcurrentModificationException} being
	 * thrown. (Please refer to notes about {@link
	 * java.util.HashSet}).
	 */
	public void delayedRemove() {
	    addPending( this );
	}

	/**
	 * Returns a String representation of this tuple.
	 */
	public String toString() {
	    StringBuffer s = new StringBuffer("<");
	    for ( int i = 0; i < fields.length; i++ ) {
		if ( i > 0 )
		    s.append( "," );
		s.append( fields[ i ] == null? "null" : fields[ i ].toString() );
	    }
	    s.append( ">" );
	    return s.toString();
	}

	/**
	 * Utility method to collect Ref objects in the fields.
	 */
	public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v) {
	    for ( int i = 0; i < fields.length; i++ ) {
		if ( fields[ i ] instanceof Ref ) {
		    Ref r = (Ref) fields[ i ];
		    if ( ! v.contains( r ) ) 
			v.add( r );
		}
	    }
	    return v;
	}

    }

    /**
     * Utility method for accessing the data table of a key pattern
     * and keying tuple.
     */
    public HashSet/*<Tuple>*/ getData(KeyPattern kp,Tuple tuple,boolean add) {
        Hashtable/*<Tuple,HashSet<Tuple>>*/ table =
	    (Hashtable/*<Tuple,HashSet<Tuple>>*/) indexes.get( kp );
	if ( table == null ) {
	    table = new Hashtable/*<Tuple,HashSet<Tuple>>*/();
	    indexes.put( kp, table );
	}
	Tuple key = kp.getKey( tuple );
        HashSet/*<Tuple>*/ data = (HashSet/*<Tuple>*/) table.get( key );
	if ( add && data == null ) {
	    data = new HashSet/*<Tuple>*/();
	    table.put( key, data );
	}
	return data;
    }
    
    /**
     * Utility method to obtain an iterator over existing key
     * patterns.
     */
    public Iterator/*<KeyPattern>*/ keyPatterns() {
	return indexes.keySet().iterator();
    }

    /**
     * Access method to add a tuple to the relation. Returns false if
     * the relation already contains the tuple. Otherwise all tables
     * are updated, and the method returns true.
     *
     * <p> Note that all values a dereferenced before being added to
     * the relation.
     *
     * @return <em>true</em> if the relation is changed, and
     * <em>false</em> otherwise.
     *
     * @see #runtimeTyping
     * @see #remove
     */
    synchronized public boolean add(Object/*...*/ [] values)
	throws ArityException, ColumnTypeException
    {
	runtimeTyping( values );

	for ( int i=0; i < values.length; i++ )
	    values[ i ] = Ref.deref( values[ i ] );

	Tuple tuple = new Tuple( values );

	HashSet/*<Tuple>*/ data = getData( main_key, tuple, true );
	if ( data.contains( tuple ) ) {
	    return false;
	}
	
	applyConstraints( tuple );

	for (Iterator/*<KeyPattern>*/ kpi = keyPatterns(); kpi.hasNext(); ) {
	    getData( (KeyPattern) kpi.next(), tuple, true ).add( tuple );
	}

	setChanged();
	notifyObservers();
	return true;
    }

    /**
     * Access method to remove a tuple from the relation. Returns
     * false if the tuple is missing from the relation. Otherwise all
     * tables are updated, and the method returns true.
     *
     * @see #runtimeTyping
     * @see #add
     */
    synchronized public boolean remove(Object/*...*/ [] values)
	throws ArityException, ColumnTypeException
    {
	if ( removeQuietly( values ) ) {
	    setChanged();
	    notifyObservers();
	    return true;
	}
	return false;
    }

    /**
     * Internal working method to remove tuples without notifying any
     * observer. The remove function is split up in this way in order
     * to allow the {@link #add} method to apply the key constraints
     * without notifications of key constraint knock-outs.
     */
    public boolean removeQuietly(Object/*...*/ [] values)
	throws ArityException, ColumnTypeException
    {
	runtimeTyping( values );
	Tuple tuple = new Tuple( values );

	HashSet/*<Tuple>*/ data = getData( main_key, tuple, true );
	if ( !data.contains( tuple ) )
	    return false;

	for (Iterator/*<KeyPattern>*/ kpi = keyPatterns(); kpi.hasNext(); ) {
	    getData( (KeyPattern) kpi.next(), tuple, true ).remove( tuple );
	}
	return true;
    }

    /**
     * Returns the size of the relation, i.e., the number of key
     * tuples in the {@link #main_key} collection.
     */
    synchronized public int size() {
        Hashtable/*<Tuple,HashSet<Tuple>>*/ table =
	    (Hashtable/*<Tuple,HashSet<Tuple>>*/) indexes.get( main_key );
	return table == null? 0 : table.size();
    }

    /**
     * Returns the projected size of the relation, i.e., the number of
     * tuples matching to the tuple of the given values, with null
     * values (or unbound Ref objects) indicating non-key columns.
     */
    public int size(Object/*...*/ [] values) {
	return size( new Tuple( values ) );
    }
    
    /**
     * Returns the projected size of the relation, i.e., the number of
     * tuples matching to the given key tuple.
     */
    synchronized public int size(Tuple key) {
	try {
	    KeyPattern kp = new KeyPattern( key.fields );
	    if ( indexes.get( kp ) == null ) {
		return 0;
	    }
	    HashSet/*<Tuple>*/ data = getData( kp, key, false );
	    return data == null? 0 : data.size();
	} catch (Exception e) {
	    e.printStackTrace();
	    return 0;
	}
    }

    /**
     * Utility method to populate a HashSet matching to a key
     */
    public void populate(KeyPattern kp)
    {
	Hashtable/*<Tuple,HashSet<Tuple>>*/ table =
	    new Hashtable/*<Tuple,HashSet<Tuple>>*/();
	indexes.put( kp, table );
        HashSet/*<Tuple>*/ db = getData( main_key, new Tuple(), true );
	for ( Iterator/*<Tuple>*/ i = db.iterator(); i.hasNext(); ) {
	    Tuple t = (Tuple) i.next();
	    HashSet/*<Tuple>*/ data = getData( kp, kp.getKey( t ), true );
	    data.add( t );
	}
    }

    /**
     * Utility method to obtain an Iterator for the tuple set that
     * matches the given key values.
     */
    public Iterator/*<Tuple>*/ query(Object/*...*/ [] values)
	throws ArityException, ColumnTypeException
    {
        runtimeTyping( values );
        Tuple key = new Tuple( values );
	return query( key );
    }

    /**
     * Utility method to obtain an Iterator for the tuple set that
     * matches the given key tuple.
     */
    synchronized public Iterator/*<Tuple>*/ query(Tuple key)
        throws ArityException, ColumnTypeException
    {
	KeyPattern kp = new KeyPattern( key.fields );
	if ( indexes.get( kp ) == null ) {
	    populate( kp );
	}
	HashSet/*<Tuple>*/ data = getData( kp, key, true );
	return data.iterator();
    }

    /**
     * The Cursor class provides Query processing support for a
     * relation.
     */
    public class Cursor implements Query {

	/**
	 * A {@link Relation.Tuple} object that is updated for new
	 * mathing values.
	 */
	public Tuple output;

	/**
	 * The querying key pattern.
	 */
	public KeyPattern key_pattern;

	/**
	 * The result iterator.
	 */
	public Iterator/*<Tuple>*/ current;

	/**
	 * Constructor for key values. Output fields are marked by
	 * {@link Ref} objects of <em>null</em> values, and ignored
	 * fields are marked by <em>null</em>.
	 */
	public Cursor(Object/*...*/ [] values)
	    throws ArityException, ColumnTypeException
	{
	    output = new Tuple( values );
	    key_pattern = new KeyPattern( output.fields );
	    reset();
	}

	/**
	 * The {@link Query#copy} method implemented by creating a new
	 * Cursor with new {@link Ref} objects.
	 */
	//@SuppressWarnings("unchecked")
	public Query copy(Vector/*<Ref>*/ newrefs) throws Exception {
	    Object [] fields = copyArray( output.fields );
	    for ( int i = 0; i < fields.length; i++ ) {
		if ( fields[ i ] instanceof Ref ) {
		    fields[ i ] = ((Ref) fields[ i ]).find( newrefs );
		}
	    }
	    return new Cursor( fields );
	}

	/**
	 * The {@link Query#reset} method implemented by recomputing
	 * the key pattern and the matching tuple set.
	 */
	public void reset()
	    throws ArityException, ColumnTypeException
	{
	    key_pattern = new KeyPattern( output.fields );
	    current = query( key_pattern.getKey( output ) );
	}

	/**
	 * The {@link Query#next} method implemented by binding the
	 * output {@link Ref} objects for the next match.
	 */
	public boolean next() {
	    if ( ! current.hasNext() ) {
		output.clear( key_pattern.pattern );
		return false;
	    }
	    ((Tuple) current.next()).getFields( key_pattern, output.fields );
	    return true;
	}

	/**
	 * The {@link Query#getRefs} method implemented by returning the
	 * {@link Ref} objects given to this Cursor object.
	 */
	public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v) {
	    return output.getRefs( v );
	}

	/**
	 * The {@link Query#addObserver} method implemented by adding
	 * the observer to the relation.
	 */
	public void addObserver(Observer x) {
	    Relation.this.addObserver( x );
	}

	/**
	 * The {@link Query#deleteObserver} method implemented by
	 * removing the observer from the relation.
	 */
	public void deleteObserver(Observer x) {
	    Relation.this.deleteObserver( x );
	}

	/**
	 * Implements the {@link Query#addable} method by verifying
	 * that all {@link Ref} objects are non-<em>null</em>.
	 */
	public boolean addable()
	{
	    return output.nullCount() == 0;
	}

	/**
	 * The {@link Query#add} method implemented by adding to
	 * current output tuple to the relation, provided that all its
	 * {@link Ref} objects are non-<em>null</em>.
	 */
	public boolean add()
	{
	    try {
		if ( output.nullCount() == 0 ) {
		    Object [] fields = new Object [ output.fields.length ];
		    System.arraycopy(
			output.fields, 0, fields, 0, fields.length );
		    return Relation.this.add( fields );
		}
	    } catch (Exception e) {
		e.printStackTrace();
		System.err.println( "** ignored **" );
	    }
	    return false;
	}

	/**
	 * Implements {@link Query#removable} by checking that the
	 * indicated tuple is stored.
	 */
	public boolean removable() {
	    HashSet/*<Tuple>*/ data = getData( main_key, output, true );
	    return data.contains( output );
	}

	/**
	 * Implements {@link Query#remove} by removing the indicated
	 * tuple. 
	 *
	 * @return <em>true</em> if tuple was removed, and
	 * <em>false</em> otherwise.
	 */
	public boolean remove() {
	    try {
		return Relation.this.remove( output.fields );
	    } catch (Exception e) {
		e.printStackTrace();
		return false;
	    }
	}
	
	/**
	 * Returns a {link java.lang.String} representation of this
	 * Cursor. This uses the {@link Relation#name} field as well
	 * as the {@link Ref#name} field of any {@link Ref} object
	 * involved (both input and output).
	 */
	public String toString() {
	    StringBuffer s = new StringBuffer();
	    s.append( Relation.this.name );
	    s.append( "(" );
	    for ( int i = 0; i < output.fields.length; i++ ) {
		if ( i > 0 )
		    s.append( "," );
		s.append( " " );
		if ( output.fields[ i ] instanceof Ref ) {
		    s.append( ((Ref) output.fields[ i ] ).getName() );
		    s.append( "=" );
		}
		s.append( output.fields[ i ] );
	    }
	    s.append( " )" );
	    return s.toString();
	}

    }

    /**
     * An interface method to obtain a {@link Query} implementation
     * object, {@link Relation.Cursor} for given key. The output
     * fields would be marked by <em>null</em> valued {@link Ref}
     * objects, which get successively assigned for the matching
     * tuples.
     */
    public Query get(Object/*...*/ [] values) throws Exception {
	return new Cursor( values );
    }

    /**
     * Interface method to obtain the Inquirable name.
     */
    public String getName() {
	return name;
    }

    //
    // Pending removal support
    //

    /**
     * Transient support for delayed removal of tuples.
     */
    public HashSet/*<Tuple>*/ pending;

    /**
     * Adds a tuple to the pending table.
     *
     * @see Relation.Tuple#delayedRemove
     */
    synchronized public void addPending(Tuple t) {
	if ( pending == null )
	    pending = new HashSet/*<Tuple>*/();
	pending.add( t );
    }

    /**
     * Utility method to remove all pending tuples. Invokes {@link
     * #processPending(boolean)} with argument <em>true</em>.
     * @return <em>true</em> if the relation is changed, and
     *<em>false</em> otherwise.
     *
     * @return <em>true</em> if the relation is changed, and
     * <em>false</em> otherwise.
     *
     * @see #processPending(boolean)
     */
    synchronized public boolean processPending()
    {
	return processPending( true );
    }

    /**
     * Utility method to remove all pending tuples. The noise argument
     * marks whether (<em>true</em>) or not (<em>false</em>) to notify
     * observers if the relation is changed.
     *
     * @return <em>true</em> if the relation is changed, and
     *<em>false</em> otherwise.
     *
     * @see #removeQuietly
     */
    synchronized public boolean processPending(boolean noise)
    {
	if ( pending == null )
	    return false;
	boolean removed = false;
	for ( Iterator/*<Tuple>*/ i = pending.iterator(); i.hasNext(); ) {
	    try {
		removed |= removeQuietly( ((Tuple) i.next()).fields );
	    } catch (Exception e) {
		System.err.println( "processPending: " + e.toString() );
	    }
	}
	pending = null;
	if ( noise && removed ) {
	    setChanged();
	    notifyObservers();
	}
	return removed;
    }

    ///
    /// Convenience methods
    ///

    /**
     * Convenience method for unary relation key constraint setting.
     */
    public void addConstraint(boolean x)
	throws ArityException {
	addConstraint( new boolean [] { x } );
    }

    /**
     * Convenience method for unary relation assertion.
     */
    public boolean add(Object x)
	throws ArityException, ColumnTypeException {
	return add( new Object [] { x } );
    }
	
    /**
     * Convenience method for unary relation retraction.
     */
    public boolean remove(Object x)
	throws ArityException, ColumnTypeException {
	return remove( new Object [] { x } );
    }


    /**
     * Convenience method for binary relation key constraint setting.
     */
    public void addConstraint(boolean x1,boolean x2)
	throws ArityException {
	addConstraint( new boolean [] { x1, x2 } );
    }

    /**
     * Convenience method for binary relation assertion.
     */
    public boolean add(Object x1,Object x2)
	throws ArityException, ColumnTypeException {
	return add( new Object [] { x1, x2 } );
    }
	
    /**
     * Convenience method for binary relation retraction.
     */
    public boolean remove(Object x1,Object x2)
	throws ArityException, ColumnTypeException {
	return remove( new Object [] { x1, x2 } );
    }


    /**
     * Convenience method for tertiary relation key constraint setting.
     */
    public void addConstraint(boolean x1,boolean x2,boolean x3)
	throws ArityException {
	addConstraint( new boolean [] { x1, x2, x3 } );
    }

    /**
     * Convenience method for tertiary relation assertion.
     */
    public boolean add(Object x1,Object x2,Object x3)
	throws ArityException, ColumnTypeException {
	return add( new Object [] { x1, x2, x3 } );
    }
	
    /**
     * Convenience method for tertiary relation retraction.
     */
    public boolean remove(Object x1,Object x2,Object x3)
	throws ArityException, ColumnTypeException {
	return remove( new Object [] { x1, x2, x3 } );
    }

    /**
     * Returns a String representation of this relation.
     */
    public String toString() {
	return indexes.toString();
    }
}
