/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Vector;
import java.util.Iterator;
import java.util.Observer;

/**
 * The Not class implements a negation-by-failure query, which is true
 * only if the wrapped {@link Query} does not provide any valid
 * bindings. The Not class also manages input bindings to ensure that
 * nothing unbound gets bound by the wrapped query.
 */
public class Not implements Query {

    /**
     * Flag to mark whether the query has been tested.
     */
    public boolean tested;

    /**
     * Holds unbound input variables.
     */
    public Vector/*<Ref>*/ unbound;

    /**
     * Holds wrapped query.
     */
    public Query query;

    /**
     * Constructor.
     */
    public Not(Query q) throws Exception {
	query = q;
	reset();
    }

    /**
     * Implements {@link Query#copy} by creating a new Not object
     * around a copy of the wrapped query.
     */
    public Query copy(Vector/*<Ref>*/ newrefs) throws Exception {
	return new Not( query.copy( newrefs ) );
    }

    /**
     * Implements {@link Query#reset} by capturing unbound Ref
     * objects, then forwarding the reset call to the wrapped query.
     */
    public void reset() throws Exception {
	unbound = new Vector/*<Ref>*/();
	for ( Iterator/*<Ref>*/ i =
		  query.getRefs( new Vector/*<Ref>*/() ).iterator();
	      i.hasNext(); ) {
	    Ref r = (Ref) i.next();
	    if ( r.get() == null )
		unbound.add( r );
	}
	tested = false;
	query.reset();
    }

    /**
     * Implements {@link Query#next} by calling on the wrapped query,
     * then remove any bindings.
     */
    //@SuppressWarnings("unchecked")
    public boolean next() throws Exception {
	if ( tested )
	    return false;
	tested = true;
	boolean b = query.next();
	for ( Iterator/*<Ref>*/ i = unbound.iterator(); i.hasNext(); ) {
	    Ref r = (Ref) i.next();
	    r.set( null );
	}
	return ! b;
    }

    /**
     * Implements {@link Query#getRefs} by forwarding to the wrapped
     * query.
     */
    public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v) {
	return query.getRefs( v );
    }

    /**
     * Implements {@link Query#addObserver} by forwarding to the
     * wrapped query.
     */
    public void addObserver(Observer x) {
	query.addObserver( x );
    }

    /**
     * Implements {@link Query#deleteObserver} by forwarding to the
     * wrapped query.
     */
    public void deleteObserver(Observer x) {
	query.deleteObserver( x );
    }

    /**
     * Implements {@link Query#addable} by calling {@link
     * Query#removable} on the wrapped query.
     */
    public boolean addable() {
	return query.removable();
    }

    /**
     * Implements {@link Query#add} by calling {@link Query#remove} on
     * the wrapped query.
     */
    public boolean add() {
	return query.remove();
    }

    /**
     * Implements {@link Query#removable} by calling {@link
     * Query#addable} on the wrapped query.
     */
    public boolean removable() {
	return query.addable();
    }

    /**
     * Implements {@link Query#remove} by calling {@link Query#add} on
     * the wrapped query.
     */
    public boolean remove() {
	return query.add();
    }

    /**
     * Returns the textual representation of the wrapped query, within
     * parentheses.
     */
    public String toString() {
	return "(" + query + ")";
    }
}
