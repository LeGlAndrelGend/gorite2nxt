/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Observer;
import java.util.Vector;

/**
 * The Query interface defines methods for query processing, to
 * support left-to-right short-cut evaluation with multiple bindings.
 *
 * <p>The sequence for query processing would typically be as follows:
 * <ol>
 *
 * <li> create the {@link Ref} objects to carry query outputs and
 * intermediate values between query conjuncts;
 *
 * <li> create the query object structure;
 *
 * <li> (optionally) add an observer on the query, in order to
 * capture triggerring from the source elements. This is only needed
 * when the following query processing is deferred or temporal in some
 * fashion;
 *
 * <li> invoke the {@link #reset} method. Most {@link Query}
 * implementations include an automatic intital {@link #reset}, and
 * therefore this call would only be needed at a second or subsequent
 * use of the query object structure;
 *
 * <li> invoke the {@link #next} method. The first invokation
 * establishes the first valid combination of bindings for the {@link
 * Ref} objects involved, and subsequent calls establish subsequent
 * valid bindings. The {@link #next} method returns <i>false</i> when
 * it exhausts the vlid combinations of bindings.
 *
 * <li> (optionally) remove the observer previosuly added.
 *
 * </ol>
 *
 * <p> The following snippet is an illustration:
 * <pre>
 * Relation r = new Relation( "parent", String.class, String.class );
 * Ref<String> p = new Ref<String>( "$parent" );
 * Ref<String> pc = new Ref<String>( "$child_parent" );
 * Ref<String> c = new Ref<String>( "$child" );
 *
 * Query grandparent = new And( r.get( p, ch ), r.get( ch, c ) );
 * while ( grandparent.next() ) {
 *     // Here r, pc and c have a valid combination of bindings
 *     System.out.println( p + " is grand parent of " + c );
 * }
 * 
 * </pre>
 */
public interface Query {

    /**
     * The reset() method is invoked in order for the Query to renew
     * itself, and provide its binding sequence from the top. The
     * Query should renew itself with regard to any changes in the
     * input.
     */
    public void reset() throws Exception;

    /**
     * The next() method is invoked in order for the Query to
     * establish the next binding in its output.
     */
    public boolean next() throws Exception;

    /**
     * The getRefs() method is invoked in order for the Query to
     * collect and report all its Ref objects.
     */
    public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v);

    /**
     * The copy method is invoked for the purpose of obtaining a deep
     * copy of a query, and in particular using new {@link Ref}
     * objects by the original names.
     */
    public Query copy(Vector/*<Ref>*/ newrefs) throws Exception;

    /**
     * The addObserver method is invoked for the purpose of adding a
     * Query processor to the observable source(s) of the query.
     */
    public void addObserver(Observer x);

    /**
     * The deleteObserver method is invoked by the Query processor in
     * order to "detach" from the observable Query source(s).
     */
    public void deleteObserver(Observer x);

    /**
     * The addable method is invoked with bound {@link Ref} objects to
     * ask whether these bindings could be added.
     */
    public boolean addable();

    /**
     * The add method is invoked with bound {@link Ref} objects in
     * order to add the current combination to all query sources, if
     * possible.
     *
     * @return <em>true</em> if any source element was updated, and
     * <em>false</em> otherwise.
     */
    public boolean add();

    /**
     * The removable method is invoked with bound {@link Ref} objects
     * to ask whether these bindings could be removed.
     */
    public boolean removable();

    /**
     * The remove method is invoked with bound {@link Ref} objects in
     * order to remove the current combination to query sources.
     *
     * @return <em>true</em> if any source element was updated, and
     * <em>false</em> otherwise.
     */
    public boolean remove();

}
