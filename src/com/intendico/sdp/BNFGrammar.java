/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.sdp;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * The BNFGrammar class is a base class for grammars that make use of
 * the LLBNF translator for defining their syntax.
 */
public class BNFGrammar extends Grammar {

    /**
     * Utility method to scan for blank or comment.
     */
    public int findBlankOrComment(CharSequence s,int from) {
	int end = s.length();
	for ( ; from < end; from++ ) {
	    if ( Character.isWhitespace( s.charAt( from ) ) )
		return from;
	    if ( s.charAt( from ) != '/' )
		continue;
	    if ( from + 1 >= end )
		return end;
	    if ( s.charAt( from + 1 ) == '*' )
		return from;
	    if ( s.charAt( from + 1 ) == '/' )
		return from;
	}
	return end;
    }

    //
    // Predefined lexical scanners.
    //

    /**
     * A Nothing is a lexical token that accepts.
     */
    public class Nothing extends Lexical {

	/**
	 * Constructor.
	 */
	public Nothing(String r) {
	    super( r );
	}

	/**
	 * Scanning nothing.
	 * Requires nothing.
	 */
	public int scan(CharSequence s,int from) {
	    return from;
	}
    }

    /**
     * An AnySymbol is a lexical token between blanks or comments.
     */
    public class AnySymbol extends Lexical {

	/**
	 * Constructor.
	 */
	public AnySymbol(String r) {
	    super( r );
	}

	/**
	 * Scanning anything unto next blank or comment or end.
	 * Requires something.
	 */
	public int scan(CharSequence s,int from) {
	    int to = findBlankOrComment( s, from );
	    return from == to? -1 : to ;
	}
    }

    /**
     * An End is a lexical token for nothing after blanks or comments.
     */
    public class End extends Lexical {

	/**
	 * Constructor.
	 */
	public End(String r) {
	    super( r );
	}

        /**
         * Requires there to remain nothing.
         */
        public int scan(CharSequence s,int from) {
	    return from == s.length()? from : -1 ;
	}
    }

    /**
     * An Identifier is a lexical token of Java identifier characters.
     */
    public class Identifier extends Lexical {

	/**
	 * Constructor.
	 */
	public Identifier(String r) {
	    super( r );
	}

	/**
	 * Scanning a Java identifier.
	 */
        public int scan(CharSequence s,int from) {
	    int end = s.length();
	    if ( from == end )
		return -1;

	    if ( ! Character.isJavaIdentifierStart( s.charAt( from++ ) ) )
		return -1;

	    for ( ; from < end; from++ ) {
		if ( ! Character.isJavaIdentifierPart( s.charAt( from ) ) )
		    return from;
	    }
	    return end;
	}
    }

    /**
     * A PackedChars is a lexical token consisting of a string of
     * characters of a given collection, in any order.
     */
    public class PackedChars extends Lexical {

	/**
	 * The accepted characters.
	 */
	public String characters;

	/**
	 * Constructor.
	 */
	public PackedChars(String r,String set) {
	    super( r );
	    characters = set;
	}

	/**
	 * Scanning a PackedChars.
	 */
        public int scan(CharSequence s,int from) {
	    int end = s.length();
	    if ( from == end )
		return -1;

	    if ( characters.indexOf( s.charAt( from++ ) ) < 0 )
		return -1;

	    for ( ; from < end; from++ ) {
		if ( characters.indexOf( s.charAt( from ) ) < 0 )
		    return from;
	    }
	    return end;
	}
    }

    /**
     * A Number is a lexical token of digits. Optionally preceded by a
     * single "-".
     */
    public class Number extends Lexical {

	/**
	 * Constructor.
	 */
	public Number(String r) {
	    super( r );
	}

	/**
	 * Scanning an integer, with an optional preceding "-".
	 */
        public int scan(CharSequence s,int from) {
            int end = s.length();

            if ( from == end )
                return -1;

	    if ( inSet( s, from, end, "+-" ) )
		from++;

	    int h = scanSet( s, from, end, "0123456789" );
	    return h == 0? -1 : from + h;
        }
    }

    /**
     * A Number is a lexical token of digits. Optionally preceded by a
     * single "-".
     */
    public class RealNumber extends Lexical {

	/**
	 * Constructor.
	 */
	public RealNumber(String r) {
	    super( r );
	}

	/**
	 * Scanning a real number, with an optional preceding "-" or '+'.
	 */
        public int scan(CharSequence s,int from) {
            int end = s.length();
	    int h = 0;
	    int f = 0;

            if ( from == end )
		return -1;

	    if ( inSet( s, from, end, "+-" ) ) {
		from++;
		if ( from == end )
		    return -1;
		h = scanSet( s, from, end, "0123456789" );
	    } else {
		if ( s.charAt( from++ ) == '0' ) {
		    // possible lead-in  for 0xh* or otherwise octal?
		    if ( from == end )
			return end;
		    if ( s.charAt( from++ ) == 'x' ) {
			// found 0x lead-in
			h = scanSet( s, from+2, end, "01234567abcdefABCDEF" );
			if ( h == 0 )
			    return -1;
			return from + h;
		    }
		    from--;
		    return from + scanSet( s, from, end, "01234567" );
		}
		from--; // 
		h = scanSet( s, from, end, "0123456789" );
		if ( h == 0 )
		    return -1;
	    }

	    from += h;
	    if ( from == end )
		return h == 0? -1 : end;

	    if ( s.charAt( from++ ) == '.' ) {
		// there's a fraction part
		f = scanSet( s, from, end, "0123456789" );
		if ( h == 0 && f == 0 )
		    return -1;
		from += f;
		if ( from >= end )
		    return end;
		if ( inSet( s, from, end, "fF" ) )
		    return from + 1;
	    } else {
		// There's no fractional part
		from--;
		if ( h == 0 )
		    return -1;
		if ( inSet( s, from, end, "lL" ) )
		    return from + 1;
	    }

	    if ( ! inSet( s, from++, end, "eE" ) )
		return from - 1;

	    // scan exponent
	    if ( inSet( s, from, end, "+-" ) )
		from++;
	    f = scanSet( s, from, end, "0123456789" );
	    return f == 0? -1 : from + f;
        }
    }

    /**
     * Scans past the given set of characters, and tells how far it
     * has scanned.
     */
    static public int scanSet(CharSequence s,int from,int end,String set) {
	int start = from;
	for ( ; from < end; from++ )
	    if ( set.indexOf( s.charAt( from ) ) < 0 )
		return from - start ;
	return from - start;
    }

    /**
     * Tells if the character in s at from is one of those in the set.
     */
    static public boolean inSet(CharSequence s,int from,int end,String set) {
	return from < end? set.indexOf( s.charAt( from ) ) >= 0 : false;
    }

    /**
     * Scan a compound character after the '\':
     * \ a    - letter or any non-digit
     * \ ooo   - octal
     * \ x hh   - hexadecimal
     * \ u nnnn - digits
     */
    static public int compound(CharSequence s,int from,int end) {
	if ( from >= end )
	    return 0;
	switch ( s.charAt( from ) ) {
	case 'x':
	    return 3;
	case 'u':
	    return 5;
	case '0': case '1': case '2': case '3':
	case '4': case '5': case '6': case '7':
	    return 3;
	default:
	    return 1;
	}
    }

    /**
     * A String is a lexical token between a mark characters
     * (double-quotes by default), but without a new-line in it.
     */
    public class QuotedString extends Lexical {

	public char mark;

        /**
         * Constructor.
         */
        public QuotedString(String r,char m) {
            super( r );
	    mark = m;
        }

        /**
         * Utility constructor to use double-quote as mark character.
         */
        public QuotedString(String r) {
            this( r, '"' );
        }

        /**
         * Scanning a String
         */
        public int scan(CharSequence s,int from) {
            int end = s.length();

            if ( from == end || s.charAt( from++ ) != mark )
                return -1;

	    tryToken( this, from ); // Special marker

            for ( ; from < end; ) {
		char c = s.charAt( from++ );
		if ( c == mark )
		    return from;
		if ( c == '\n' )
		    return -1; // bad string
		if ( c == '\\' ) {
		    // lead-in for compound characters.
		    from += compound( s, from, end );
		    if ( from > end )
			return -1;
		}
            }
	    // Bad string 2
            return -1;
        }

	/**
	 * The error name for a buggy string.
	 */
	public String errorName() {
	    return "terminated and well-formed <" + rule + ">" ;
	}
    }

    /**
     * A QuotedCharacter is a lexical token between a pair of
     * single-quotes, with restrictions.
     */
    public class QuotedCharacter extends Lexical {

        /**
         * Constructor.
         */
        public QuotedCharacter(String r) {
            super( r );
        }

        /**
         * Scanning a String
         */
        public int scan(CharSequence s,int from) {
            int end = s.length();

            if ( from == end || s.charAt( from++ ) != '\'' )
                return -1;

	    tryToken( this, from ); // Special marker

	    if ( from == end )
		return -1;

	    switch ( s.charAt( from++ ) ) {
	    case '\'':
		return -1;
	    case '\\':
		from += compound( s, from, end );
		break;
	    default:
		from += 1;
		break;
	    }
	    if ( from >= end )
		return -1;
	    if ( s.charAt( from++ ) != '\'' )
		return -1;
            return from;
        }

	/**
	 * The error name for a buggy string.
	 */
	public String errorName() {
	    return "terminated and well-formed <" + rule + ">" ;
	}
    }


    /**
     * A lexical scanner to match multi-word phrases from a given
     * collection. It greedily consumes its longest match.
     */
    public class PhraseSet extends Lexical {

	/**
	 * Matching phrases. This contains the successive heads of the
	 * matching phrases, mapped to 0, 1, or 2. 0 means that the
	 * phrase ends there, 1 means it must continue, and 2 means it
	 * may end or continue. The scanner always consumes as much as
	 * possible.
	 */
	public TreeSet/*<String>*/ phrases = new TreeSet/*<String>*/();

	/**
	 * Cache of defining phrases.
	 */
	public Vector/*<String>*/ definition = new Vector/*<String>*/();

	/**
	 * Constructor for a given name and collection of phrases.
	 */
	public PhraseSet(String name,Vector/*<String>*/ set) {
	    super( name );
	    addSet( set );
	}

	/**
	 * Utility method to add a collection of phrases.
	 */
	public void addSet(Vector/*<String>*/ set) {
	    for ( Iterator si = set.iterator(); si.hasNext(); ) {
		String phrase = (String) si.next();
		definition.add( phrase );
		phrases.add( phrase.toLowerCase() );
	    }
	}

	/**
	 * Returns index to first character following a matching
	 * token.
	 */
	public int scan(CharSequence input,int from) {
	    CharSequence phrase = input.subSequence( from, input.length() );
	    if ( from >= input.length() )
		return -1;
	    char first = phrase.charAt( 0 );
	    for ( Iterator/*<String>*/ pi =
		      phrases.headSet( phrase, true ).descendingIterator();
		  pi.hasNext(); ) {
		String key = (String) pi.next();
		if ( key.charAt( 0 ) != first ) {
		    System.err.println( "failed: " + key );
		    return -1;
		}
		if ( startsWith( phrase, 0, key ) ) {
		    return from + key.length();
		}
	    }
	    return -1;
	}

	/**
	 * Returns this collection as a defintion clause:
	 * name = { .... }
	 */
	public String toString() {
	    StringBuilder s = new StringBuilder();
	    s.append( rule );
	    s.append( " = {" );
	    String separator = "\n  ";
	    //for ( String phrase : definition ) {
	    for ( Iterator pi = phrases.iterator(); pi.hasNext(); ) {
		String phrase = (String) pi.next();
		s.append( separator );
		s.append( phrase );
		separator = ",\n  ";
	    }
	    s.append( "\n}\n" );
	    return s.toString();
	}
    }

    //
    // Predefined utility actions
    //

    /**
     * Utility method to add elements individually from a Vector
     * operand, if it begins with the operator.
     */
    static public void addArguments(Vector args,Object op,Object opnd) {
	if ( opnd instanceof Vector ) {
	    Vector v = (Vector) opnd;
	    if ( v.size() > 0 && op.equals( v.get( 0 ) ) ) {
		v.remove( 0 );
		args.addAll( v );
		return;
	    }
	}
	args.add( opnd );
    }

    /**
     * Helper class for infix syntax
     */
    public class Infix extends Production {

	/**
	 * Default Infix action is to move the middle element to
	 * beginning, if there are three arguments, and otherwise
	 * re-use the Grammar.Production action.
	 */
	public Object action(Vector v) throws Exception {
	    if ( v.size() != 3 )
		return super.action( v );

	    v.add( 0, v.remove( 1 ) );
	    return v;
	}
    }

    /**
     * Helper class for postfix syntax
     */
    public class Postfix extends Production {

	/**
	 * Default Postfix action is to move the last element to
	 * beginning, if there are more than one argument, and otherwise
	 * re-use the Grammar.Production action.
	 */
	public Object action(Vector v) throws Exception {
	    if ( v.size() <= 1 )
		return super.action( v );

	    v.add( 0, v.remove( v.size() - 1 ) );
	    return v;
	}
    }

    /**
     * Helper class for associative infix syntax
     */
    public class Associative extends Production {

	/**
	 * Default Associative action is to move the middle element to
	 * beginning, if there are three arguments, and then flatten
	 * out the first and last argument of they are vectors with
	 * the same operator. If there are not three arguments, then 
	 * re-use the Grammar.Production action.
	 */
	public Object action(Vector v) throws Exception {
	    if ( v.size() != 3 )
		return super.action( v );

	    Object op = v.get( 1 );
	    Vector args = new Vector();
	    args.add( op );
	    addArguments( args, op, v.get( 0 ) );
	    addArguments( args, op, v.get( 2 ) );
	    return args;
	}
    }

    /**
     * Helper class for lists
     */
    public class Enlist extends Production {

	/**
	 * Default Enlist action is coalesc arguments into a Vector.
	 */
	public Object action(Vector v) {
	    if ( v.size() <= 1 )
		return v;

	    Object last = v.remove( v.size() - 1 );
	    if ( last instanceof Vector ) {
		v.addAll( (Vector) last );
	    } else {
		v.add( last );
	    }
	    return v;
	}
    }

    /**
     * Helper class for left-associative lists
     */
    public class Leftlist extends Production {

	/**
	 * Default Enlist action is coalesc arguments into a ".."
	 * headed Vector.
	 */
	public Object action(Vector v) {
	    if ( v.size() <= 1 )
		return v;

	    Object first = v.remove( 0 );

	    if ( first instanceof Vector ) {
		((Vector) first).addAll( v );
		return first;
	    }

	    v.add( 0, first );
	    return v;
	}
    }

    /**
     * Installs a grammar from string rules using LLBNF.
     */
    public void install(String rules) {
	try {
	    new LLBNF().addRules( this, rules );
	} catch (Throwable t) {
	    t.printStackTrace();
	    throw new Error( "Grammar failure" );
	}
    }

    /**
     * Utility method to clip a string after some length
     */
    public String clip(String input,int clip) {
	int x = input.indexOf( "\n", 0 );
	if ( x > 0 && x < clip )
	    return input.substring( 0, x ) + " ..." ;
	if ( clip < input.length() )
	    return input.substring( 0, clip ) + "...";
	return input;
    }

    /**
     * Utility method to time parse and process.
     */
    public boolean timedProcess(int i,String goal,String input) {
	try {
	    System.out.println(
		"(" + i + ") " + goal + ": " + clip( input, 65 ) );
	    long time = System.currentTimeMillis();
	    ParseTree pr = parse( goal, input );
	    long snap = System.currentTimeMillis() - time;
	    if ( pr != null ) {
		//System.out.println( "--> " + pr );
		time = System.currentTimeMillis();
		Object result = process( pr );
		time = System.currentTimeMillis() - time;
		System.out.println( "==> " + result );
		System.out.println(
		    "--------- time = " + snap + "/" + time + " ms" );
		return true;
	    } else {
		System.out.println( lastError( input ) );
		System.out.println(
		    "--------- time = " + snap + "/-- ms" );
		return false;
	    }
	} catch (Throwable t) {
	    t.printStackTrace();
	}
	return false;
    }

    /**
     * Utility method to parse and process several tests.
     */
    public boolean processAll(String [][] args) {
	
	for ( int i=0; i<args.length; i++ ) {
	    if ( ! timedProcess( i, args[i][0], args[i][1] ) )
		return false;
	}
	return true;
    }

}
